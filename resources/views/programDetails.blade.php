<title>Program</title>
<style>
    .whysection{
        background-image: url({{ asset('assets/images/programDetailsSideImg.png')}}),url({{ asset('assets/images/programDetailsWhyBg.png') }});
        background-position: right center, center center;
        background-repeat: no-repeat, repeat;
        background-size: 40%, 100%;
    }
    .imageBg{
        background-color: #F2F0F4
    }
    .accordion-button:not(.collapsed) {
        color: #fff!important;
        background-color: #FF6100!important;
        box-shadow: inset 0 -1px 0 rgba(0,0,0,.125);
    }
</style>
@include('include.header')

<div class="content">
    <div class="top_menu_header">
        @include('include.top_menu')
    </div>
</div>
<section class="mt-4 pt-5">
    <iframe src="https://www.youtube.com/embed/{{$course_details->video}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="" style="height: 300px; width: 100%;"></iframe>
</section>
<section class="mt-5 whysection">
    <div class="container-fluid">
        <div class="ps-md-5">
            <div class="row">
                <div class="col-md-4">
                    <p class="text-start mt-md-5">
                        <span class="btn2">Class {{$course_details->class}}</span>
                    </p>
                    <h1>Why {{$course_details->name}}?</h1>
                    <p class="">
                        {{$course_details->why}}
                    </p>
                    {{-- <p class="my-md-5">
                        <span class="btn1">Register Now</span>
                    </p> --}}
                </div>       
            </div>    
        </div>
    </div>
    <div class="container-fluid pb-5">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center">
                    &nbsp;
                </h1>
                <p class="text-center">
                    &nbsp;
                </p>
            </div>
        </div>
    </div>    
</section>
<section class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center fw-bold">How Does It <span class="col1"> Work?</span></h1>
                <p class="text-center">
                    {{$course_details->how}}
                </p>
            </div>
        </div>
        <div class="row justify-content-center align-items-center">
            @foreach($deliverables as $deliverable)  
                @if(($deliverable->type)=="howProgram")                      
                    <div class="col-md-3">
                        <div class="howCard text-center pb-4">
                            <img src="https://files.edudigm.com/Files/{{$deliverable->images}}" class="img-fluid" alt="">
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</section>
<section class="mt-5">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <h1 class="text-center fw-bold">{{$course_details->name}} <span class="col1"> Offers</span></h1>
            </div>
            <div class="col-12">
                <h5 class="text-center fw-bold">Why you should enroll?</h5>
            </div>            
        </div>
        <div class="row justify-content-center align-items-center">
            @foreach($deliverables as $deliverable)  
                @if(($deliverable->type)=="whyProgram")
                    <div class="col-md-4 mb-4">
                        <div class="card py-2 px-4">
                            <p class="text-center mb-0">
                                <img src="https://files.edudigm.com/Files/{{$deliverable->images}}" class="w-25" alt="">
                            </p>
                            <p class="text-center">{{$deliverable->name}}</p>
                        </div>
                    </div>
                @endif
            @endforeach            
        </div>
        <div class="row">
            <div class="col-12">
                <p class="text-center">{{$course_details->what}}</p>
            </div>
        </div>
    </div>
</section>
<section class="mt-md-5">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <h1 class="text-center fw-bold"><span class="col1"> Impact</span></h1>
            </div>          
        </div>
        <div class="row justify-content-center align-items-center">
            @foreach($deliverables as $deliverable)  
                @if(($deliverable->type)=="impactProgram")                      
                    <div class="col-md-3">
                        <div class="howCard text-center pb-4">
                            <img src="https://files.edudigm.com/Files/{{$deliverable->images}}" class="img-fluid" alt="">
                        </div>
                    </div>
                @endif
            @endforeach         
        </div>
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <p class="text-center">
                    {{$course_details->impact}}
                </p>
            </div>          
        </div>
        
    </div>
<section class="mt-md-5">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <h1 class="text-center fw-bold"><span class="col1"> Next Steps</span></h1>
            </div>          
        </div>
        <div class="row justify-content-center align-items-center">
            <div class="col-md-12 mt-4">
                <p class="text-center">
                    {{$course_details->next_steps}}
                </p>
            </div>          
        </div>
        
    </div>
</section>
<section class="mt-md-5">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <h1 class="text-center fw-bold"><span class="col1"> FAQs</span></h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <div class="accordion" id="accordionFlushExample">                    
                    @foreach($faqs as $faq)                        
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="heading{{$faq->id}}">
                                <span class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">
                                    {{$faq->question}}
                                </span>
                            </h2>
                            <div id="collapse{{$faq->id}}" class="accordion-collapse collapse" aria-labelledby="heading{{$faq->id}}"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    {{$faq->answer}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-12">
            @include('include.commonSection', ['type' => 0])
        </div>
    </div>
</div>
<div class="bottom_footer_menu">
    @include('include.bottom_footer')
</div>
@include('include.footer')
<script>
    
</script>