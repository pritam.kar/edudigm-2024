<title>Program</title>
@include('include.header')
<?php $studentClass= Request::get('class')  ?>
<div class="content">
    <div class="top_menu_header">
        @include('include.top_menu')
    </div>
</div>
<section class="mt-4 pt-5">
    <iframe src="https://www.youtube.com/embed/sRLtmP1Nzys?si=_SiMajQYMi3XPBru" title="YouTube video player"
        frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
        allowfullscreen="" style="height: 300px; width: 100%;"></iframe>
</section>
<section class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h5 class="fw-bold">
                    Edudigm’s programs can be divided into two categories: Guided Learning Programs named after
                    scientists like Einstein, Newton, Curie & Tesla. These programs differ from one another on the basis
                    of what is included under them as a part of their offerings (Yes indicates that the product/service
                    is offered and No indicates that it isn’t)
                </h5>
            </div>
        </div>
    </div>
</section>
<section class="mt-5">
    <div class="container journey">
        <div class="row align-items-center justify-content-center">
            <div class="col-md-10">
                <h1 class="text-center fw-bold">Guided Learning <span class="col1">Program</span></h1>
            </div>
        </div>
        <div class="row align-items-center justify-content-center">
            <div class="col-md-4">
                <img src="{{ asset('assets/images/guidedLearningProgram.png') }}" class="img-fluid" alt="">
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                @include('include.ClassProgram', ['type' => 'gl','studentClass' => $studentClass])
            </div>
        </div>
    </div>
</section>
<section class="mt-5">
    <div class="container journey">
        <div class="row align-items-center justify-content-center">
            <div class="col-md-10">
                <h1 class="text-center fw-bold">Self-Paced Learning <span class="col1">Program</span></h1>
            </div>
        </div>
        <div class="row align-items-center justify-content-center">
            <div class="col-md-4">
                <img src="{{ asset('assets/images/selfPacedLearningProgram.png') }}" class="img-fluid" alt="">
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                @include('include.ClassProgram', ['type' => 'spl','studentClass' => $studentClass])
            </div>
        </div>
    </div>
</section>
@include('include.commonSection')
<div class="bottom_footer_menu">
    @include('include.bottom_footer')
</div>
@include('include.footer')