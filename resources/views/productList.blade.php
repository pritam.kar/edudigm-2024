<title>Home</title>
@include('include.header')
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
<style>
    .ui-slider {
        position: relative;
        text-align: left;
    }

    .ui-slider-horizontal {
        height: 0.8em;
    }

    .ui-widget-content {
        border: 1px solid #dddddd;
        background: #ffffff;
        color: #333333;
    }

    .ui-slider-horizontal .ui-slider-range {
        top: 0;
        height: 100%;
    }

    .ui-slider .ui-slider-range {
        position: absolute;
        z-index: 1;
        font-size: .7em;
        display: block;
        border: 0;
        background-position: 0 0;
    }

    .ui-widget-header {
        border: 1px solid #dddddd;
        background: #e9e9e9;
        color: #333333;
        font-weight: bold;
    }

    .ui-slider-horizontal .ui-slider-handle {
        top: -0.3em;
        margin-left: -0.6em;
    }

    .ui-slider .ui-slider-handle {
        position: absolute;
        z-index: 2;
        width: 1.2em;
        height: 1.2em;
        cursor: pointer;
        -ms-touch-action: none;
        touch-action: none;
    }

    .ui-state-default,
    .ui-widget-content .ui-state-default,
    .ui-widget-header .ui-state-default,
    .ui-button,
    html .ui-button.ui-state-disabled:hover,
    html .ui-button.ui-state-disabled:active {
        border: 1px solid #c5c5c5;
        background: #f6f6f6;
        font-weight: normal;
        color: #454545;
    }

    .product_card {
        box-shadow: 0px 0px 20px -5px rgba(0, 0, 0, 0.25);
        padding: 15px 15px;
        border-radius: 10px;
    }

    .product_card .item img {
        border-radius: 10px 10px 0px 0px;
    }

    a {
        text-decoration: none;
        color: initial;
    }

    a:hover {
        color: initial;
    }

    .pro_descp {
        max-height: 100px;
        min-height: 100px;
        overflow: hidden;
    }

    .item {
        max-height: 150px;
        min-height: 150px;
        overflow: hidden;
        text-align: center;
    }

    .filterFixed {
        position: fixed;
        width: 23%;
        background-color: #fff;
    }

    .grid_view,
    .list_View {
        font-size: 25px;
        cursor: pointer;
    }
</style>
<script>
    var minAmt=0;
    var maxAmt={{$data->maxPrice}};
    var selectedMinAmt=0;
    var selectedMaxAmt={{$data->maxPrice}};
    $(function () {
      $("#slider-range").slider({
        range: true,
        min: minAmt,
        max: maxAmt,
        step: 50,
        values: [{{$data->pricefrom ?? 0}}, {{$data->priceto ?? $data->maxPrice}}],
        slide: function (event, ui) {
            selectedMinAmt=ui.values[0];
            selectedMaxAmt=ui.values[1];
            $("#minAmt").text("₹"+ui.values[0])
            $("#maxAmt").text("₹"+ui.values[1])
        }
      });
      $("#minAmt").text("₹"+minAmt);
      $("#maxAmt").text("₹"+maxAmt);
    });
</script>
<div class="content">
    <div class="top_menu_header">
        @include('include.top_menu')
    </div>
</div>
<div class="container-fluid mt-5 pt-5">
    <div class="row">
        <div class="col-md-3">
            <div class="filterFixed">
                <div class="row justify-content-center align-items-center">
                    <div class="col-12 mb-4">
                        <h1 class="col1 text-center">Filters</h1>
                    </div>
                </div>
                <div class="row justify-content-center align-items-center">
                    <div class="col-md-12 mb-4">
                        <select name="" class="form-select" id="ddl_class">
                            <option value="">Select Class</option>
                            <option value="III">3</option>
                            <option value="IV">4</option>
                            <option value="V">5</option>
                            <option value="VI">6</option>
                            <option value="VII">7</option>
                            <option value="VIII">8</option>
                            <option value="IX">9</option>
                            <option value="X">10</option>
                            <option value="XI">11</option>
                            <option value="XII">12</option>
                            <option value="XII">12+</option>
                        </select>
                    </div>
                    <div class="col-md-12 mb-4">
                        <select name="" class="form-select" id="ddl_Category">
                            <option value="">Select Category</option>
                        </select>
                    </div>
                    <div class="col-md-12 mb-4">
                        <p class="mb-0">Price</p>
                        <p class="mb-4">
                            <span id="minAmt" class="float-start"></span>
                            <span id="maxAmt" class="float-end"></span>
                        </p>
                        <div id="slider-range"></div>
                        <p class="mt-4 text-center">
                            <span class="btn1 py-1" id="btn_priceFilter">Apply</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="row align-items-center justify-content-center">
                <div class="col-6">
                    <p class="text-start">
                        Showing {{count($data->products)}} Product
                    </p>
                </div>
                <div class="col-6">
                    <p class="text-end">
                        <span title="View As Grid" class="grid_view" style="display: none">
                            <i class="fas fa-th"></i>
                        </span>
                        <span title="View As List" class="list_View">
                            <i class="fas fa-bars"></i>
                        </span>
                    </p>
                </div>
            </div>
            <div class="row justify-content-center align-items-center" id="listAndGridView">
                @if(count($data->products)>0)
                    @foreach($data->products as $product)                        
                        <?php
                            $productNameUrl = str_replace(' ', '-', $product->name); 
                            $firstImage = explode(',', $product->images)[0];
                            $firstImage = str_replace('rcb_images/', '', $firstImage);
                        ?>
                        <div class="col-md-4 mb-4 overallWidth">
                            <div class="product_card">
                                <div class="row">
                                    <div class="col-md-12 imageWidth">
                                        <div class="item">
                                            <a href="/product-details/{{$productNameUrl}}?id={{$product->id}}">
                                                @if($firstImage)
                                                <img src="https://files.edudigm.com/Files/rcb_images/{{$firstImage}}" alt=""
                                                    class="img-fluid">
                                                @else
                                                <img src="{{ asset('assets/images/soon.jpg') }}" alt="" class="img-fluid w-50">
                                                @endif
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-12 descriptionWidth">
                                        <div class="pro_detail pro_detail mt-3 mx-3">
                                            <a href="/product-details/{{$productNameUrl}}?id={{$product->id}}">
                                                <h6 class="mb-1 fw-bold text-dark">{{$product->name}} (Class
                                                    {{$product->class}})</h6>
                                            </a>
                                            <a href="/product-details/{{$productNameUrl}}?id={{$product->id}}">
                                                <p class="mb-1 pro_descp">
                                                    {{$product->short_desc}}
                                                </p>
                                            </a>
                                            @if ($product->discount && $product->discount > 0)
                                            <p class="mb-1">
                                                <span class="price me-1">
                                                    <s><i class="fas fa-rupee-sign"></i>{{$product->price}}</s>
                                                </span>
                                                <span class="text-success me-1">({{$product->discount}}% off)</span>
                                                <span class="text-uppercase prices h4">
                                                    <i class="fas fa-rupee-sign"></i> {{
                                                    $product->price-($product->price*$product->discount/100) }}
                                                </span>
                                            </p>
                                            @else
                                            <p class="mb-1">
                                                <span class="text-uppercase prices h4">
                                                    <i class="fas fa-rupee-sign"></i>{{$product->price}}
                                                </span>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12 buttonWidth">
                                        <div class="pro_action_btn mt-3 mx-3">
                                            {{-- <p class="text-center mt-4">
                                                <span class="btn1">Buy Now</span>
                                            </p> --}}
                                            <p class="text-center mt-4">
                                                <span class="btn1" onclick="addToCart({{$product->id}})">Add To Cart</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <h1 class="text-muted text-center">Product Will Be Added Soon</h1>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="container mt-5">
    <div class="row">
        <div class="col-12">
            @include('include.Contact')
        </div>
    </div>
</div>
<div class="bottom_footer_menu">
    @include('include.bottom_footer')
</div>
@include('include.footer')
@include('include.operationCart')
<script>
    var catHtml="";
    var tempAllCategories = localStorage.getItem("allCategories");
    if(tempAllCategories){
        populateAllCategoried(JSON.parse(tempAllCategories));
    }else{
        allCategories()
    }    
    async function allCategories() {
        const response = await fetch("{{ route('getAllCategories') }}");
        const allCat = await response.json();
        populateAllCategoried(allCat);
        if(allCat){
            localStorage.setItem("allCategories",JSON.stringify(allCat));
        }
    }
    function populateAllCategoried(allCat){
        if(allCat){
            allCat.forEach((element,index) => {
                if(element.name!="Program"){
                    catHtml+='<option value="'+element.id+'">'+element.name+'</option>';
                }
            });            
            $('#ddl_Category').append(catHtml);
        }
    }
    $('.list_View').click(function (e) {
        var url=window.location.href;
        var newUrl=removeParam("view", url);
        window.location.href = newUrl+'&view=List';
    });
    $('.grid_view').click(function (e) {
        var url=window.location.href;
        var newUrl=removeParam("view", url);
        window.location.href = newUrl+'&view=Grid';
    });
    $('#btn_priceFilter').click(function (e) {      
        var url=window.location.href;
        var pricefrom=removeParam("pricefrom", url);
        var newUrl=removeParam("priceto", pricefrom);
        window.location.href = newUrl+'&pricefrom='+selectedMinAmt+'&priceto='+selectedMaxAmt;
    });
</script>
<script>
    $(document).ready(function () {
        var categoryid=getUrlParam("categoryid");
        var selectedClass=getCookie("selectedClass");
        if(categoryid){
            $('#ddl_Category').val(categoryid);
        }
        if(selectedClass){
            $('#ddl_class').val(selectedClass);
        }
        var viewAs=getUrlParam('view');
        if(viewAs){
            if(viewAs=="List"){
                $('#listAndGridView .overallWidth').removeClass('col-md-4').addClass('col-md-12');
                $('#listAndGridView .imageWidth').removeClass('col-md-12').addClass('col-md-4');
                $('#listAndGridView .descriptionWidth').removeClass('col-md-12').addClass('col-md-6');
                $('#listAndGridView .buttonWidth').removeClass('col-md-12').addClass('col-md-2');
                $('.grid_view').show();
                $('.list_view').hide();
            }else if(viewAs=="Grid"){
                $('#listAndGridView .overallWidth').addClass('col-md-4').removeClass('col-md-12');
                $('#listAndGridView .imageWidth').addClass('col-md-12').removeClass('col-md-4');
                $('#listAndGridView .descriptionWidth').addClass('col-md-12').removeClass('col-md-6');
                $('#listAndGridView .buttonWidth').addClass('col-md-12').removeClass('col-md-2');
                $('.grid_view').hide();
                $('.list_view').show();
            }
        }
    });
    $('#ddl_class').change(function (e) {
        var selectedClass=$(this).val();
        setCookie('selectedClass', selectedClass, 365);
        window.location.reload()        
    });
    $('#ddl_Category').change(function (e) {
        var selectedCategory=$(this).val();
        var selectedCategoryText=($('#ddl_Category :selected').text()).replace(/ /g,"-");
        window.location.href = "/product-list/"+selectedCategoryText+"?categoryid="+selectedCategory;
    });
</script>