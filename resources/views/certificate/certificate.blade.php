<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$CertificateGenerator['website']}} | {{$CertificateGenerator['product_name']}} | {{$CertificateGenerator['event_name']}}</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <!-- Font CSS -->
    <link rel="stylesheet" href="https://use.typekit.net/lpz2xld.css">
    <!-- Font CSS -->
    <!-- FontAwesome CSS -->
    <link href="https://kit-pro.fontawesome.com/releases/v5.15.4/css/pro.min.css" rel="stylesheet">
    <!-- FontAwesome CSS -->
        {!!$CertificateGenerator['certificate_css']!!}
</head>

<body>
    {!!$CertificateGenerator['certificate_html']!!}
    <div class="modal fade" id="mdl_certificate" tabIndex="-1" data-bs-backdrop="static" data-bs-keyboard="false"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="margin: 0 auto;">Please Provide The
                        Details Below</h5>
                </div>
                <div class="modal-body">
                    <input type="text" name="" class="form-control mb-3" placeholder="Name" id="name" />
                    <select name="" class="form-control mb-3" id="class">
                        <option value="">Please Select Class</option>
                        <option value="I">I</option>
                        <option value="II">II</option>
                        <option value="III">III</option>
                        <option value="IV">IV</option>
                        <option value="V">V</option>
                        <option value="VI">VI</option>
                        <option value="VII">VII</option>
                        <option value="VIII">VIII</option>
                        <option value="IX">IX</option>
                        <option value="X">X</option>
                        <option value="XI">XI</option>
                        <option value="XII">XII</option>
                    </select>
                    <input type="text" name="" class="form-control mb-3" placeholder="School" id="school" />
                    <input type="tel" name="" class="form-control mb-3" placeholder="Mobile Number" maxLength="10"
                        id="mobile" />
                    <input type="password" name="" class="form-control mb-3 btncerticficate" placeholder="OTP"
                        maxLength="4" id="mobileOtp" />
                    <p class="mb-0 text-center">
                        <span class="text-danger" id="errorText"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btncerticficate" onclick="validateOtp()">Verify
                        OTP</button>
                    <button type="button" class="btn btn-primary" id="btnotp" onclick="generateOtp()">Generate
                        OTP</button>

                    <div>
                        <button type="button" class="btn btn-primary" id="btnresend" onclick="resendOTP()">Resend
                            OTP</button>
                        <p class="mb-0">Resend OTP In <span id="otpTimerText"></span> Sec</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</body>
<!-- Bootstrap CSS -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
    integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
    integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
</script>
<!-- Bootstrap CSS -->
<!-- Jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<!-- Jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.4.1/html2canvas.min.js"
    integrity="sha512-BNaRQnYJYiPSqHHDb58B0yaPfCu+Wgds8Gp/gU33kqBtgNS4tSPHuGibyoeqMV/TJlSKda6FXzoEyYGjTe+vXA=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        var otptimerVal = 60;
        var otpTimer=0;
        var element = $("#certificateDiv");
        var getCanvas;
        const myInterval="";
        $(document).ready(function () {
            $('#certificateDiv').hide();
            $('.btncerticficate').hide();
            $('#btnresend').hide();
            $('#otpTimerText').parent().hide();
            setTimeout(() => {
                $('#mdl_certificate').modal('show');
            }, 100);
            // seteventDate(allDate[id]);
            $('.slccertificateDate').text("April 05, 2024")
        });
        function validateOtp() {
            var stuname = $('#name').val();
            var stuclass = $('#class').val();
            var stuschool = $('#school').val();
            var stumobile = $('#mobile').val();
            var otpsss = $('#mobileOtp').val();
            if (stuname && stuclass && stuschool && stumobile && stumobile.length == 10 && otpsss && otpsss.length == 4) {
                var payLoad = {
                    mobile: stumobile,
                    otp: otpsss
                }
                var verifyOtp_res=verifyOtp(payLoad);
                if (verifyOtp_res.response_code == 200) {
                    $('#certificateDiv').show();
                    $('#errorText').text("");
                    $('.slccertificateName').text(stuname)
                    $('.slccertificateClass').text(stuclass)
                    $('.slccertificateSchool').text(stuschool)
                    $('#mdl_certificate').modal('hide');
                    printPage();
                } else {
                    $('#errorText').text(verifyOtp_res.response_msg)
                }
            } else {
                $('#errorText').text("Please Fill The Above Fields Properly")
            }
        }
        function generateOtp() {
            var stuname = $('#name').val();
            var stuclass = $('#class').val();
            var stuschool = $('#school').val();
            var stumobile = $('#mobile').val();
            if (stuname && stuclass && stuschool && stumobile && stumobile.length == 10) {
                var payLoad = {
                    mobile: stumobile,
                    name: stuname,
                    class: stuclass,
                    school: stuschool
                }
                var sendOtp_res=sendOtp(payLoad);
                if (sendOtp_res.response_code == 200) {
                    $('#errorText').text("");
                    $('.btncerticficate').show();
                    $('#btnresend').show();
                    $('#btnotp').hide();
                    otpTimer=otptimerVal;
                    clearInterval(myInterval);
                    myInterval = setInterval(myTimer, 1000);
                } else {
    
                }
            } else {
                $('#errorText').text("Please Fill The Above Fields Properly")
            }
        }
        function resendOTP() {        
            generateOtp();
        }
        const printPage = (u) => {
            $('.btn_print').hide();
            setTimeout(() => {
                window.print();
            }, 500);
            // setTimeout(() => {
            //     html2canvas($("#certificateDiv")[0]).then((canvas) => {
            //         // console.log(canvas);
            //         //$('#certificateDiv').hide()
            //         $("#root").append(canvas);
            //         var link = document.createElement("a");
            //         document.body.appendChild(link);
            //         link.download = "Mech_to_Dev.jpg";
            //         link.href = canvas.toDataURL();
            //         link.target = '_blank';
            //         link.click();
            //     });
            // }, 100);
        }
        function myTimer() {
            if (otpTimer > 0) {
                otpTimer = otpTimer - 1;
                $('#otpTimerText').parent().show();
                $('#otpTimerText').text(otpTimer);
                $('#btnresend').hide();
            }else{
                $('#btnresend').show();
                $('#otpTimerText').parent().hide();            
            }
        }    
        function sendOtp(alldata) {
            return $.parseJSON($.ajax({
                type: "POST",
                url: "https://crm.edudigm.com/api/certificate-send-otp",
                data: JSON.stringify(alldata),
                dataType: "json",
                async: !1,
                headers: {
                    "Content-Type": "application/json"
                }
            }).responseText)
        }
        function verifyOtp(alldata) {
            return $.parseJSON($.ajax({
                type: "POST",
                url: "https://crm.edudigm.com/api/certificate-verify-otp",
                data: JSON.stringify(alldata),
                dataType: "json",
                async: !1,
                headers: {
                    "Content-Type": "application/json"
                }
            }).responseText)
        }
    </script>

</html>