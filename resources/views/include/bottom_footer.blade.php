<style>
    .bottom_footer_menu {
        background-color: var(--col1)
    }

    .footer h4 {
        color: #F55D00;
        background-color: #fff;
        display: inline-block;
        padding: 2px 22px;
        border-radius: 20px;
        font-weight: bold;
    }

    .footer {
        position: relative;
    }

    #lamp {
        width: 200px;
        position: absolute;
        transform: scaleX(-1);
        top: -160px;
    }

    .social i {
        font-size: 30px;
        color: #fff;
        padding: 10px 15px;
        transition: all 0.3s ease-in-out;
    }

    .social i:hover {
        color: #000;
    }

    .social a,.footer a {
        text-decoration: none !important;
        color: #fff;
    }

    .pub_pay_now {
        background-color: #FF6100 !important;
        color: #fff !important;
        border: solid 1px #fff !important;
    }

    @media(max-width:576px) {
        .link {
            margin-top: -46px;
        }

        .social_icon {
            margin-bottom: 50px;
        }

        .footer_img {
            margin-top: 80px;
        }

        .link {
            text-align: center;
        }

        .pub_pay_now {
            background-color: transparent !important;
            border: solid 1px #fff !important;
            color: #fff !important;
        }
    }

    @media(max-width:300px) {
        .pub_pay_now {
            background-color: transparent !important;
            border: solid 1px #fff !important;
            color: #fff !important;
        }
    }


    .footer {
        margin: 200px 0 0 0;
    }
</style>

<section class="footer pb-5">
    <div class="container-fluid">
        <div class="pt-5">
            <div class="container">
                <div class="footer_img">
                    <img src="{{ asset('assets/images/footerlamp.svg') }}" id="lamp" alt="">
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="row align-items-baseline justify-content-center">
                            <div class="col-md-4 col-sm-6 link mt-1">
                                <h4 class="text-start mt-md-5 Lato ">Join Us</h4>
                                <p><a href="/contact">Contact Us</a></p>

                                {{-- <div class="row">
                                    <div class="col-12">
                                        <p id="email" class="text-start Lato">Email:
                                            <a href="mailto:contact@edudigm.in" class="Lato">contact@edudigm.in</a>
                                        </p>
                                        <p id="number" class="text-start Lato">Mobile:
                                            <a href="tel:+916290820499" class="Lato">+91-6290820499 </a>
                                        </p>
                                        <p class="text-start Lato" id="address">
                                            Office: Ambuja Ecostation, 12th floor, Suite 1202, 64 BP, Sector 5,
                                            Saltlake, Kolkata-
                                            700091
                                        </p>
                                    </div>
                                </div> --}}
                            </div>
                            <div class="col-md-4 col-sm-6 link mt-1">
                                {{-- <h4 class="text-start mt-md-5 Lato ">About Us</h4>
                                <p id="team" class="Lato mb-0"><a href="javascript:void(0)">Udayer Pathey</a></p>
                                <p id="team" class="Lato mb-0"><a href="javascript:void(0)">Team</a></p> --}}
                            </div>
                            <!-- <div class="col-md-4 nh_contacts" style="display:none;">
                                <div class="contact">
                                    <h4 class="text-start mt-5 Lato ">Udayer Pathey</h4>
                                    <p class="fw-normal Lato">Edudigm Co-ordinator :- <br> <span class="fw-bold"> Mr. Soumyadeep Kundu </span> </p>
                                    <p class="fw-normal Lato">Mobile: <a href="tel:+916290820499">+91-6290820499</a></p>
                                    <p class="fw-normal Lato">Narayana Hrudalaya Co-ordinator :- <br> <span class="fw-bold"> Mr.Akhaya Mohanthy </span></p>
                                    <p class="fw-normal Lato">Mobile: <a href="tel:+919007094860">+91-9007094860 </a></p>
                                    <p class="fw-normal Lato">Email: <a href="mailto:contact@edudigm.in">akhaya.mohanthy@narayanahealth.org</a></p>
                                    <p class="fw-normal Lato">RN Tagore Hospital, Kolkata</p>
                                </div>
                            </div> -->
                            <div class="col-md-4 col-sm-12 link mt-1">
                                {{-- <h4 class="text-start mt-md-5 Lato ">Policy</h4>
                                <p id="privacy" class="Lato mb-0"><a href="javascript:void(0)">Privacy Policy</a></p>
                                <p id="term" class="Lato mb-0"><a href="javascript:void(0)">Terms & Condition</a></p>
                                <p id="refund" class="Lato mb-0"><a href="javascript:void(0)">Refund Policy</a></p> --}}
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6 text-end">
                        <div class="row mt-5">
                            <div class="col-12 text-center">
                                <a href="https://play.google.com/store/apps/details?id=com.edudigm.eklavya"
                                    style="cursor:pointer;">
                                    <img src="{{asset('assets/images/play_store.png')}}" alt="">
                                </a>
                            </div>
                            <div class="col-12 mt-4 text-center">
                                <span class="social mb-4">
                                    <a href="https://www.facebook.com/edudigm.education/">
                                        <img src="{{asset('assets/images/facebook.png')}}" alt="" style="width:50px;">
                                    </a>
                                    <a href="https://www.youtube.com/channel/UCzN6m3nzuhGEVaDGLI8peQg/videos">
                                        <img src="{{asset('assets/images/youtube.png')}}" alt="" style="width:50px;">
                                    </a>
                                    <a
                                        href="https://www.linkedin.com/company/edudigm-education-services-private-limited/mycompany/">
                                        <img src="{{asset('assets/images/linkedin.png')}}" alt="" style="width:50px;">
                                    </a>
                                    <a href="https://www.instagram.com/edudigm/">
                                        <img src="{{asset('assets/images/instagram.png')}}" alt="" style="width:50px;">
                                    </a>
                                </span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
</script>