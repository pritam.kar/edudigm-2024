<style>
    .ContactCard {
        box-shadow: 3px 4px 18px -7px rgba(0, 0, 0, 0.25);
        background: linear-gradient(90deg, #ffffff 50%, var(--col1) 50%);
        border-radius: 15px;
    }
    @media  only screen and (max-device-width:768px) {
        .ContactCard {
            background: linear-gradient(180deg, #ffffff 65%, var(--col1) 50%)
        }
    }
    #ContactCaptcha{
        text-align: center;
    }

    
</style>

<div class="ContactCard px-md-5 py-4">
    <div class="row justify-content-center align-items-center">
        <div class="col-md-6">
            <p>
                <img src="{{ asset('assets/images/logo.png') }}" class="w-50" alt="">
            </p>
            <input type="hidden" name="" id="txt_ContactPageLink">
            <div class="form-group mb-3">
                <input type="text" name="" class="form-control text-dark emptyValidate" id="txt_ContactName"
                    placeholder="Enter Your Full Name">
            </div>
            <div class="form-group mb-3">
                <input type="tel" class="form-control mobile text-dark emptyValidate mobile" id="txt_ContactMobile"
                    onkeypress="return (event.charCode !=8 &amp;&amp; event.charCode ==0 || (event.charCode >= 48 &amp;&amp; event.charCode <= 57))"
                    name="mobile" maxlength="10" placeholder="Enter 10 Digit Mobile Number" autocomplete="off">
            </div>
            <div class="form-group mb-3">
                <input type="email" class="form-control email text-dark emptyValidate email" id="txt_ContactEmail"
                    name="email" aria-describedby="email" placeholder="Enter Your Valid Email Id">
            </div>
            <div class="form-group mb-3">
                <textarea class="form-control text-dark emptyValidate" id="txt_ContactQuery"
                    placeholder="Enter Your Query"></textarea>
            </div>
            <div class="form-group mb-3">
                <div id="ContactCaptcha"></div>
                <input type="text" name="" id="txtValidateCaptcha" class="form-control">
            </div>
            <div class="form-group mb-3">
                <p class="text-center">
                    <span class="btn1" id="btn_SaveContact">Send Message</span>
                </p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="bg1 px-4">
                <h1 class="text-light mb-4">
                    Our Team will get in touch with you soon...
                </h1>
                <p class="">
                    <i class="fas fa-phone-alt text-light me-2"></i>
                    <a href="tel:+916290820499" class="text-light" target="_blank">+91-6290820499</a>
                </p>
                <p class="">
                    <i class="fas fa-envelope text-light me-2"></i>
                    <a href="mailto:contact@edudigm.in" class="text-light" target="_blank">contact@edudigm.in</a>
                </p>
                <p class="">
                    <i class="far fa-map-marker-alt text-light me-2"></i>
                    <span class="text-light">
                        12th floor, Suite 1202, 64 BP, Sector 5, Saltlake, Kolkata- 700091
                    </span>
                </p>
            </div>
        </div>
    </div>
</div>
<script>
    $('#txt_ContactPageLink').val(window.location.href);
    
    $(document).on('click','#btn_SaveContact',function(){
        var ContactName = $('#txt_ContactName').val();
        var ContactMobile = $('#txt_ContactMobile').val();
        var ContactEmail = $('#txt_ContactEmail').val();
        var ContactQuery = $('#txt_ContactQuery').val();
        var ContactPageLink = $('#txt_ContactPageLink').val();
        var ValidateCaptcha = $('#txtValidateCaptcha').val();
        if(code==ValidateCaptcha){
            if(ContactName && ContactMobile && ContactEmail && ContactQuery && ContactPageLink){
                var token="{{ csrf_token() }}";
                var allData={
                    "ContactName":ContactName,
                    "ContactMobile":ContactMobile,
                    "ContactEmail":ContactEmail,
                    "ContactQuery":ContactQuery,
                    "ContactPageLink":ContactPageLink,
                }
                postData("{{ route('storeContactUs') }}", allData,"POST",token).then((data) => {
                    $('#txt_ContactName').val("");
                    $('#txt_ContactMobile').val("");
                    $('#txt_ContactEmail').val("");
                    $('#txt_ContactQuery').val("");
                    $('#txtValidateCaptcha').val("");
                    createCaptcha()
                    toast("suc","Your query has been posted successfully. We will get back to you soon.")
                });
            }else{
                toast("err","All Fields Are Mandatory")
            }
        }else{
            toast("err","Invalid Captcha")
        }
    });
    var code;
    createCaptcha()
    function createCaptcha() {
        //clear the contents of captcha div first 
        document.getElementById('ContactCaptcha').innerHTML = "";
        var charsArray =
        "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var lengthOtp = 6;
        var captcha = [];
        for (var i = 0; i < lengthOtp; i++) {
            //below code will not allow Repetition of Characters
            var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
            if (captcha.indexOf(charsArray[index]) == -1)
            captcha.push(charsArray[index]);
            else i--;
        }
        var canv = document.createElement("canvas");
        canv.id = "captcha";
        canv.width = 100;
        canv.height = 35;
        var ctx = canv.getContext("2d");
        ctx.font = "25px Georgia";
        ctx.strokeText(captcha.join(""), 0, 30);
        //storing captcha so that can validate you can save it somewhere else according to your specific requirements
        code = captcha.join("");
        document.getElementById("ContactCaptcha").appendChild(canv); // adds the canvas to the body element
        $('#ContactCaptcha').append('<span class="ms-4" id="regenarateCaptcha"><i class="far fa-sync"></i></span>')
    }
    $(document).on('click','#regenarateCaptcha',function(){
        createCaptcha()
    })
</script>