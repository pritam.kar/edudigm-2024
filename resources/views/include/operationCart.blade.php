<style>
    .cartIcon{
        position: fixed;
        top: 50%;
        right: 0;
        transform: translate(0, -50%);
        font-size: 25px;
        background-color: var(--col1);
        display: inline-block;
        width: 50px;
        height: 50px;
        border-radius: 50%;
        text-align: center;
        line-height: 50px;
        color: #fff;
    }
</style>
<script>
    function addToCart(productId){        
        var allCart=JSON.parse(localStorage.getItem('allCart'))
        if(allCart){
            var findIndex=null;
            allCart.forEach((element,index) => {
                if(element.productId==productId){
                    findIndex=index;
                }
            });
            if(findIndex!=null){
                allCart[findIndex].qty=(allCart[findIndex].qty)+1;
            }else{
                var cartSave={
                    "productId":productId,
                    "qty":1
                }
                allCart.push(cartSave);
            }            
            localStorage.setItem('allCart',JSON.stringify(allCart))
        }else{
            var cartSave=[{
                "productId":productId,
                "qty":1
            }]
            localStorage.setItem('allCart',JSON.stringify(cartSave))
        }
        toast("Suc","Added To Cart")
    }
</script>
<a href="{{ route('cart') }}" class="">
    <i class="far fa-shopping-cart cartIcon"></i>
</a>