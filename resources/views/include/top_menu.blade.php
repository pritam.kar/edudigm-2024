<section>
    <style>
        .navbar-brand img {
            width: 150px;
        }

        #navbar {
            background-color: #FF6100;
        }

        .nav-link {
            color: #ffffff !important;
        }
        .nav-link.active{
            font-weight: bold;
            text-decoration: underline;
            font-size: 1.1rem;
        }
    </style>
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark" id="navbar">
        <div class="container-fluid">
            <a class="navbar-brand" href="/"> <img src="{{ asset('assets/images/edu_logo_white.png') }}" alt=""
                    class="logo"></a>
            <div class="nh_logo"></div>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                @include('include.menu')
            </div>
        </div>
    </nav>
</section>
<script>
    $('.nav-link').each(function (index, element) {
        var attrLink=$(this).attr('active_link');
        // if(attrLink==""){
        //     $(this).addClass('active');
        // }else{
            var valOfUrl=window.location.href.indexOf(attrLink);
            if(valOfUrl!=-1){
                if(attrLink){
                    $(this).addClass('active');
                }
            }
        // }
        
    });
</script>