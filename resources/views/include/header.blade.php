<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-138349849-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-138349849-1');
    </script>

    {{-- Bootstrap CSS Start --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    {{-- Bootstrap CSS End --}}
    {{-- Fonts Start --}}
    <link rel="stylesheet" href="https://use.typekit.net/lpz2xld.css">    
    {{-- Fonts End --}}
    {{-- FontAwesome Start --}}
    <link href="https://kit-pro.fontawesome.com/releases/v5.15.4/css/pro.min.css" rel="stylesheet">
    {{-- FontAwesome End --}}
    {{-- jQuery Start --}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    {{-- jQuery End --}}
    {{-- Validation JS Start --}}
    <script src="{{ asset('assets/js/all.js') }}"></script>
    {{-- Validation JS End --}}
    {{-- Toast Start --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" integrity="sha512-wJgJNTBBkLit7ymC6vvzM1EcSWeM9mmOu+1USHaRBbHkm6W9EgM0HY27+UtUaprntaYQJF75rc8gjxllKs5OIQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    {{-- Toast End --}}
    <link rel="icon" href="{{ asset('assets/images/favicon.png') }}"><!-- 32×32 -->
	<link rel="icon" href="{{ asset('assets/images/favicon.png') }}" type="image/svg+xml">
	<link rel="apple-touch-icon" href="{{ asset('assets/images/favicon.png') }}">
    <link href="{{ asset('assets/css/all.css') }}" rel="stylesheet">
    <script>
        var baseUrl="https://crm.edudigm.com/api/";
    </script>
</head>
<body>