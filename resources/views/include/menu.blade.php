<ul class="navbar-nav navbar-nav ms-auto" style="align-items: baseline;">
    <li class="nav-item">
        <a class="nav-link" active_link="" href="/">Home</a>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" active_link="program" href="#" id="navbarDropdown" role="button"
            data-bs-toggle="dropdown" aria-expanded="false">
            Programs
        </a>
        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="/program/III?class=III">III</a></li>
            <li><a class="dropdown-item" href="/program/IV?class=IV">IV</a></li>
            <li><a class="dropdown-item" href="/program/V?class=V">V</a></li>
            <li><a class="dropdown-item" href="/program/VI?class=VI">VI</a></li>
            <li><a class="dropdown-item" href="/program/VII?class=VII">VII</a></li>
            <li><a class="dropdown-item" href="/program/VIII?class=VIII">VIII</a></li>
            <li><a class="dropdown-item" href="/program/IX?class=IX">IX</a></li>
            <li><a class="dropdown-item" href="/program/X?class=X">X</a></li>
            <li><a class="dropdown-item" href="/program/XI?class=XI">XI</a></li>
            <li><a class="dropdown-item" href="/program/XII?class=XII">XII</a></li>
            <li><a class="dropdown-item" href="/program/XII+?class=XII+">XII+</a></li>
        </ul>
    </li>
    <li class="nav-item">
        <a class="nav-link Lato" active_link="pradip" href="/product-list/Publications?categoryid=2">Products</a>
    </li>
    <ul style="list-style-type: none;padding: 0;display: flex;" id="allOtherMenu">
        
    </ul>
    <li class="nav-item">
        <a class="nav-link Lato" active_link="pradip" href="/contact">Contact</a>
    </li>
</ul>
<script>
    var allMenuRes=[];
    var allMenu=sessionStorage.allmenu;
    if(allMenu){
        allMenuRes=JSON.parse(allMenu);
    }else{
        allMenuRes=getresponsedata();
        sessionStorage.allmenu=JSON.stringify(allMenuRes);
    }
    if(allMenuRes.length>0){
        var menuHtml="";
        allMenuRes.forEach(element => {
            var str = element.replace('-', ' ').toLowerCase();
            menuHtml+='<li class="nav-item">';
            menuHtml+='    <a class="nav-link Lato" active_link="pradip" href="/pages/'+element+'" style="text-transform: capitalize;">'+str+'</a>';
            menuHtml+='</li>';
        });
        $('#allOtherMenu').html(menuHtml)
    }
    function getresponsedata() {
        return $.parseJSON(
            $.ajax({
                type: "GET",
                url: "{{ route('getAllmenu') }}",
                dataType: "json",
                async: !1
            }).responseText
        )
    }
</script>