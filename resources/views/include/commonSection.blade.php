<style>
    .spanone,
    .spantwo,
    .sliderMenu .images img {
        display: none
    }

    .sentence {
        position: absolute;
        bottom: 0;
        width: 100%;
        text-align: center;
        color: #fff;
    }

    :root {
        --col1: #FF6100;
    }

    * {
        font-family: "Poppins", sans-serif;
    }

    .bg1 {
        background-color: var(--col1);
    }

    .col1 {
        color: var(--col1);
    }

    .topmenuLogo {
        width: 150px;
    }

    .navbar {
        transition: 0.4s;
    }

    .nav-link.active {
        font-weight: 700;
    }

    .headerText {
        font-size: 3vw;
        text-align: center;
        height: 60px;
        overflow: hidden;
        margin-top: -150px;
        color: #fff;
    }

    @media only screen and (max-device-width:500px) {
        .headerText {
            margin-top: -8% !important;
            height: auto;
        }

        .clientPartnerImages {
            width: 100% !important;
            height: 100% !important;
        }
    }

    .headerText .withText {
        margin-left: 15vw;
    }

    .leftRightText .para {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        font-size: 3vw;
        text-align: center;
        font-weight: 600;
    }

    .leftRightText .para span {
        font-size: 5vw;
    }

    .btn_primary {
        display: inline-block;
        background-color: var(--col1);
        font-size: 3vw;
        padding: 1vw 3vw;
        border-radius: 15px;
        color: #fff;
        cursor: pointer;
        text-decoration: none;
        border: 1px solid var(--col1);
        transition: all ease .3s;
    }

    .btn_primary:hover {
        color: var(--col1);
        background-color: #fff;
    }

    .toughBoring .para {
        font-size: 2.5vw;
    }

    .toughBoaringCard {
        background-color: #F2F0F4;
        padding: 2vw 2vw;
        border-radius: 15px;
    }

    .toughBoaringCard img {
        width: 50%;
    }

    .journey .para {
        font-weight: 600;
    }

    .allEduStoreItems img {
        background-color: var(--col1);
        padding: 15px;
        border-radius: 15px;
        width: 100px;
    }

    .eduStarBg {
        background: linear-gradient(144deg, rgba(255, 97, 1, 1) 0%, rgba(225, 165, 57, 1) 50%);
        position: relative;
        padding: 15px 0 45px 0;
        border-radius: 15px;
    }

    .eduStarBg .btn_viewMore {
        position: absolute;
        bottom: 0;
        width: 100%;
        text-align: center;
        background-color: #00000078;
        border-radius: 0px 0px 15px 15px;
        color: #fff;
        padding: 5px 0;
        cursor: pointer;
    }

    .statsCard {
        background-color: #fff;
        border-radius: 10px;
    }

    .statsBg {
        background-size: 250%;
        background-repeat: no-repeat;
        background-position: center;
    }

    .imgStatsPerson {
        position: absolute;
        width: 150px;
        bottom: 0;
    }

    .stats_person1 {
        left: 5%;
    }

    .stats_person2 {
        right: 5%;
    }

    .clientPartnerImages {
        width: 100px;
        height: 100px;
        margin-bottom: 25px;
        filter: grayscale(100%);
        transition: all ease .5s;
    }

    .clientPartnerImages:hover {
        filter: grayscale(0%);
    }

    .awardsImages {
        max-width: 50%;
        position: relative;
    }
</style>
<div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-3"><img src="{{ asset('assets/images/edustore_logo.png') }}" alt="" class="img-fluid"></div>
    </div>
    <div class="row align-items-center justify-content-center">
        <div class="col-md-8">
            <p class="text-center fw-bold">Explore EduStore, the gateway to all the offerings of Edudigm categorized
                as follows</p>
        </div>
    </div>
</div>
@include('include.storeCategory',['studentClass' => ""])
<div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-4">
            <p class="text-center"><img src="{{ asset('assets/images/star.png') }}" alt="" class="w-50"></p>
            <h1 class="text-center fw-bold">Our <span class="col1">Edustars</span></h1>
        </div>
    </div>
</div>
<div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-12">
            <div class="eduStarBg">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-4">
                        <p class="text-center mb-0">
                            <img src="{{ asset('assets/images/allOtherImages/student1.png') }}" alt="" class="w-50">
                        </p>
                        <p class="text-center mb-0 text-light fw-bold mt-4">Abhishek Bose</p>
                        <p class="text-center mb-0 text-light fw-bold">at TESLA</p>
                    </div>
                    <div class="col-md-4">
                        <p class="text-center mb-0">
                            <img src="{{ asset('assets/images/allOtherImages/student2.png') }}" alt="" class="w-50">
                        </p>
                        <p class="text-center mb-0 text-light fw-bold mt-4">Haimoshree Das</p>
                        <p class="text-center mb-0 text-light fw-bold">at MIT Boston</p>
                    </div>
                    <div class="col-md-4">
                        <p class="text-center mb-0">
                            <img src="{{ asset('assets/images/allOtherImages/student3.png') }}" alt="" class="w-50">
                        </p>
                        <p class="text-center mb-0 text-light fw-bold mt-4">Shruti Chakraborty</p>
                        <p class="text-center mb-0 text-light fw-bold">at CERN</p>
                    </div>
                </div>{{-- <span class="btn_viewMore">View More</span> --}}
            </div>
        </div>
    </div>
</div>
<div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-8">
            <p class="text-center fw-bold">Our students are studying/working in the top colleges/companies of the
                world like Tesla, Apple, Google, Amazon, Tata, ITC along with the IITs, AIIMs, BITS.</p>
        </div>
    </div>
</div>
<div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-6"><img src="{{ asset('assets/images/company_logos.png') }}" alt="" class="img-fluid"></div>
    </div>
</div>
<div class="statsBg position-relative py-5" style="background-image: url({{ asset('assets/images/stats_bg.png') }});">
    <img src="{{ asset('assets/images/stats_person1.png') }}" alt="" class="imgStatsPerson stats_person1"><img
        src="{{ asset('assets/images/stats_person2.png') }}" alt="" class="imgStatsPerson stats_person2">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-md-12">
                <h1 class="text-center text-light mb-5">OUR STATS</h1>
            </div>
            <div class="col-md-12">
                <div class="row align-items-center justify-content-center" id="counter">
                    <div class="col-md-3 text-center">
                        <div class="statsCard py-5 h3">
                            <p class="counter-value mb-0 col1 h1"><span data-count="15"></span>+</p> Years
                        </div>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="statsCard py-5 h3">
                            <p class="counter-value mb-0 col1 h1"><span data-count="50000"></span>+</p> Students
                        </div>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="statsCard py-5 h3">
                            <p class="counter-value mb-0 col1 h1"><span data-count="1000"></span>+</p> Schools
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-4">
            <h1 class="text-center fw-bold">Partners / <span class="col1">Clients</span></h1>
        </div>
    </div>
</div>
<div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-12">
            <div class="row align-items-center justify-content-center" id="clientDiv">
                
            </div>
        </div>
    </div>
</div>
<div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-10">
            <h1 class="text-center fw-bold">Awards &amp; <span class="col1">Recognitions</span></h1>
        </div>
    </div>
</div>
<div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-12">
            <div class="row align-items-center justify-content-center" id="awardDiv">
                
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('include.Contact')
        </div>
    </div>
</div>
{{-- <div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-12">
            <h1 class="text-center fw-bold">FAQ<span class="col1">s</span></h1>
        </div>
    </div>
</div>
<div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-12">
            <div class="accordion accordion-flush" id="accordionFlushExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingOne">
                        <p class="accordion-button mb-0" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                            Accordion Item #1
                        </p>
                    </h2>
                    <div id="flush-collapseOne" class="accordion-collapse collapse show"
                        aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">Placeholder content for this accordion, which is intended to
                            demonstrate the <code>.accordion-flush</code> class. This is the first item's accordion
                            body.</div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingTwo">
                        <p class="accordion-button collapsed mb-0" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                            Accordion Item #2
                        </p>
                    </h2>
                    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo"
                        data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">Placeholder content for this accordion, which is intended to
                            demonstrate the <code>.accordion-flush</code> class. This is the second item's accordion
                            body. Let's imagine this being filled with some actual content.</div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingThree">
                        <p class="accordion-button collapsed mb-0" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-collapseThree" aria-expanded="false"
                            aria-controls="flush-collapseThree">
                            Accordion Item #3
                        </p>
                    </h2>
                    <div id="flush-collapseThree" class="accordion-collapse collapse"
                        aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">Placeholder content for this accordion, which is intended to
                            demonstrate the <code>.accordion-flush</code> class. This is the third item's accordion
                            body. Nothing more exciting happening here in terms of content, but just filling up the
                            space to make it look, at least at first glance, a bit more representative of how this would
                            look in a real-world application.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<script>
    var a = 0;
    $(window).scroll(function () {
        var oTop = $('#counter').offset().top - window.innerHeight;
        if (a == 0 && $(window).scrollTop() > oTop) {
            $(".counter-value span").each(function(){var t=$(this),n=t.attr("data-count");$({countNum:t.text()}).animate({countNum:n},{duration:3e3,easing:"swing",step:function(){t.text(Math.floor(this.countNum))},complete:function(){t.text(this.countNum)}})});
            a = 1;
        }
    });
    var allImageRes=[];
    var allImages=sessionStorage.allImages;
    if(allImages){
        allImageRes=JSON.parse(allImages);
    }else{
        allImageRes=getallImagedata();
        sessionStorage.allImages=JSON.stringify(allImageRes);
    }
    if(allImageRes.length>0){
        var clientHtml="";
        var awardHtml="";
        allImageRes.forEach(element => {
            if(element.type=="Partners"){
                clientHtml+='<div class="col-2">';
                clientHtml+='    <p class="text-center mb-0">';
                clientHtml+='        <img src="https://files.edudigm.com/Files/'+element.image+'" alt="" class="clientPartnerImages">';
                clientHtml+='    </p>';
                clientHtml+='</div>';
            }
            if(element.type=="Awards"){
                awardHtml+='<div class="col-2">';
                awardHtml+='    <p class="text-center mb-0">';
                awardHtml+='        <img src="https://files.edudigm.com/Files/'+element.image+'" alt="" class="clientPartnerImages">';
                awardHtml+='    </p>';
                awardHtml+='</div>';
            }
        });
        $('#clientDiv').html(clientHtml);
        $('#awardDiv').html(awardHtml);
    }
    function getallImagedata() {
        return $.parseJSON(
            $.ajax({
                type: "GET",
                url: "{{ route('getAllImages') }}",
                dataType: "json",
                async: !1
            }).responseText
        )
    }
</script>