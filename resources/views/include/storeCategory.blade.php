<style>
    .allEduStoreItems img {
        background-color: var(--col1);
        padding: 15px;
        border-radius: 15px;
        width: 100px;
    }
</style>

<div class="container my-4 allEduStoreItems">
    <div class="row align-items-center justify-content-center" id="allCategoriesData"></div>
    </div>
</div>
<script>
    var catHtml="";
    var tempAllCategories = localStorage.getItem("allCategories");
    if(tempAllCategories){
        populateAllCategoried(JSON.parse(tempAllCategories));
    }else{
        allCategories()
    }
    
    async function allCategories() {
        const response = await fetch("{{ route('getAllCategories') }}");
        const allCat = await response.json();
        populateAllCategoried(allCat);
        if(allCat){
            localStorage.setItem("allCategories",JSON.stringify(allCat));
        }
    }
    function populateAllCategoried(allCat){
        if(allCat){
            allCat.forEach((element,index) => {
                if(element.name!="Program"){
                    var linkName=element.name.replace(/ /g,"-");
                    catHtml+='<div class="col-md-3 text-center">';
                    catHtml+='    <a href="/product-list/'+linkName+'?categoryid='+element.id+'">';
                    catHtml+='        <img src="https://files.edudigm.com/Files/'+element.images+'" alt="" class="img-fluid">';
                    catHtml+='        <p class="fw-bold mb-0 mt-2">'+element.name+'</p>';
                    catHtml+='        <p>'+element.description+'</p>';
                    catHtml+='    </a>';
                    catHtml+='</div>';
                }
            });            
            $('#allCategoriesData').html(catHtml);
        }
    }
</script>