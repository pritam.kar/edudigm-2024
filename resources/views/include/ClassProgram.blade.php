<style>
    .classprogramcard {
        background-color: #FFF6F1;
        border-radius: 15px;
        padding: 20px 20px;
    }

    .classlist {
        text-align: center;
    }

    .classlist li {
        list-style-type: none;
        display: inline-block;
        border: 1px solid var(--col1);
        padding: 10px 20px;
        margin: 5px 10px;
        background-color: #fff;
        border-radius: 10px;
        color: #878787;
        cursor: pointer;
        font-weight: bold;
    }

    .gradienttext {
        background: linear-gradient(91deg, #ff6100 69.77%, #ffaa10 101.94%);
        -webkit-background-clip: text;
        background-clip: text;
        color: transparent;
    }

    .programcard {
        background-color: #FFECD0;
        border: 1px solid #FF995A;
        border-radius: 10px;
    }

    .programcard img {
        width: 100px;
    }

    .tagName,
    .notes {
        font-size: 12px
    }

    .tagName i {
        width: 20px;
    }

    .classlist .active {
        background-color: #E2FFEA;
        border: 1px solid #7BDA94;
    }
</style>
<div class="classprogramcard">
    <ul class='classlist'>
        <li classValue="III">3</li>
        <li classValue="IV">4</li>
        <li classValue="V">5</li>
        <li classValue="VI">6</li>
        <li classValue="VII">7</li>
        <li classValue="VIII">8</li>
        <li classValue="IX">9</li>
        <li classValue="X">10</li>
        <li classValue="XI">11</li>
        <li classValue="XII">12</li>
        <li classValue="XII+">12 Pass</li>
    </ul>

    @if($type =='0')
        <div class="programs">
            <h2 class="text-center gradienttext fw-bold">Guided Learning Programs</h2>
            <div class="row guidedDiv"></div>
        </div>
        <div class="programs mt-5">
            <h2 class="text-center gradienttext fw-bold">Self-Paced Learning Programs</h2>
            <div class="row selfpaced"></div>
        </div>
    @elseif($type =='gl')
        <div class="programs">
            <h2 class="text-center gradienttext fw-bold">Guided Learning Programs</h2>
            <div class="row guidedDiv"></div>
        </div>
    @elseif($type =='spl' || $type == 0)
        <div class="programs">
            <h2 class="text-center gradienttext fw-bold">Self-Paced Learning Programs</h2>
            <div class="row selfpaced"></div>
        </div>
    @endif
</div>
<script>
    var studentClass;
    $(document).ready(function () {
        studentClass=getUrlParam("class");
        if(!studentClass){
            studentClass=getCookie("selectedClass");
        }
        if(studentClass){
            $(".classlist").find(`[classValue='`+studentClass+`']`).click();
        }else{
            $(".classlist").find(`[classValue='III']`).click();
        }
    });
    $(document).on('click','.classlist li',function(){
        var selectedClass=$(this).attr("classValue");
        setCookie('selectedClass', selectedClass, 365);
        $('.classlist li').removeClass('active');
        $(".classlist").find(`[classValue='`+selectedClass+`']`).addClass('active');
        populateProgramCard();
    });
    function populateProgramCard(){
        var studentClass=getCookie("selectedClass");
        var guidedHtml="";
        var selfpacedHtml="";
        postDataNoDataToken("/get-all-program/"+studentClass,"GET").then((data) => {
            if(data.length>0){
                data.forEach(element => {
                    var filename=(element.name).toLowerCase();
                    var price=element.reg_fee+element.seat_booking_fee+element.variable_fees;
                    var genHtml="";
                    genHtml+='<div class="col-md-3 mb-3">';
                    genHtml+='    <div class="programcard py-4">';
                    genHtml+='        <p class="text-center">';
                    genHtml+='            <img src="/assets/images/'+filename+'.png" alt="" />';
                    genHtml+='        </p>';
                    genHtml+='        <p class="text-center">';
                    genHtml+='            <span>'+element.name+'</span>';
                    genHtml+='        </p>';
                    genHtml+='        <p class="text-center">';
                    genHtml+='            <span class="fw-bold">₹'+price+'</span>';
                    genHtml+='        </p>';
                    genHtml+='        <p class="text-center mt-3">';
                    if(selfPaced.includes(element.course_mode_id)){
                        genHtml+='<a href="/admission?type=spl&class='+studentClass+'" class="btn1 readMore">Buy Now</a>';
                    }
                    if(guided.includes(element.course_mode_id)){
                        genHtml+='<a href="/admission?type=gl&class='+studentClass+'" class="btn1 readMore">Buy Now</a>';
                    }
                    genHtml+='        </p>';
                    genHtml+='        <p class="text-center mt-3">';
                    genHtml+='            <a href="/program-details/'+element.id+'" class="readMore"><u>Read More</u></a>';
                    genHtml+='        </p>';
                    genHtml+='        <div class="px-4">';
                    genHtml+='            <p class="tagName mb-0 text-center">';
                    genHtml+=                element.prog_mode;
                    genHtml+='            </p>';
                    genHtml+='            <p class="tagName mt-2 mb-0 text-center">';
                    genHtml+=              '<b>Subject Covered: </b>'+  subject(element.subjects_covered);
                    genHtml+='            </p>';
                    // genHtml+='            <p class="tagName mb-0 text-center">';
                    // genHtml+='                <i class="far fa-times text-danger"></i> Lorem Ipsum Dolor 2';
                    // genHtml+='            </p>';
                    genHtml+='        </div>';
                    genHtml+='        <p class="text-center fw-bold mt-4">Price/Student/Year**</p>';
                    // genHtml+='        <div class="px-4">';
                    // genHtml+='            <p class="notes">';
                    // genHtml+='                *We set up STEM labs in schools based on budget, availability of space and student strength.';
                    // genHtml+='            </p>';
                    // genHtml+='            <p class="notes">';
                    // genHtml+='                **These are heavily discounted prices offered to school partners committed to STEM Learning.';
                    // genHtml+='            </p>';
                    // genHtml+='        </div>';
                    genHtml+='    </div>';
                    genHtml+='</div>';
                    if(selfPaced.includes(element.course_mode_id)){
                        selfpacedHtml+=genHtml;
                    }
                    if(guided.includes(element.course_mode_id)){
                        guidedHtml+=genHtml;
                    }
                });
                $('.guidedDiv').html(guidedHtml);
                $('.selfpaced').html(selfpacedHtml);
                $("img").on("error", function () {
                    $(this).css("opacity", "0");
                });
            }
        });
    }
    
</script>