<title>Edudigm | Cart</title>
<style>
    .cartImage {
        max-width: 200px;
    }

    .deleteItem {
        position: absolute;
        right: 0;
        cursor: pointer;
    }
</style>
@include('include.header')
<div class="content">
    <div class="top_menu_header">
        @include('include.top_menu')
    </div>
</div>
<div class="container mt-5 pt-5">
    <div class="row" id="cartData"></div>
    <div class="row" id="totalprice">
        <div class="col-md-6 text-center">
            <h1 class="text-center">Total Price: <span id="totalPrice"></span></h1>
        </div>
        <div class="col-md-6 text-center">
            <span class="btn1" data-bs-toggle="modal" data-bs-target="#mdl_proceedToPay">Proceed To Pay</span>
        </div>
    </div>

</div>
<div class="container mt-5">
    <div class="row">
        <div class="col-12">
            @include('include.Contact')
        </div>
    </div>
</div>
<div class="bottom_footer_menu">
    @include('include.bottom_footer')
</div>
@include('include.footer')
<div class="modal fade" id="mdl_proceedToPay" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body position-relative">
                <button type="button" class="btn-close position-absolute end-0" data-bs-dismiss="modal" aria-label="Close"></button>
                <p class="text-center fw-bold">
                    Please Provide The Below Details
                </p>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="user_details_frm">
                            <div class="col-12 mb-3">
                                <input type="text" class="form-control" id="publication_txt_name" placeholder="Enter Your Full Name" autocomplete="off">
                            </div>
                            <div class="col-12 mb-3">
                                <input type="email" class="form-control email" id="publication_txt_email" aria-describedby="emailHelp" placeholder="Enter Your Valid Email">
                                <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                            </div>
                            <div class="col-12 mb-3">
                                <input type="text" class="form-control mobile" id="publication_txt_mobile" maxlength="10" onkeypress="return (event.charCode !=8 &amp;&amp; event.charCode ==0 || (event.charCode >= 48 &amp;&amp; event.charCode <= 57))" aria-describedby="phoneHelp" placeholder="Enter Your Valid Mobile">
                                <div id="phoneHelp" class="form-text">We'll never share your phone number with anyone else.</div>
                            </div>
                            <div class="col-12 mb-3">
                                <input type="hidden" value="700099" class="form-control zipcode" id="publication_txt_zipcode" maxlength="6" minlength="6" onkeypress="return (event.charCode !=8 &amp;&amp; event.charCode ==0 || (event.charCode >= 48 &amp;&amp; event.charCode <= 57))" aria-describedby="phoneHelp" placeholder="Enter Your Valid Postal Code">
                            </div>
                            <div class="col-12 mb-3">
                                <textarea rows="3" class="form-control" id="publication_address" placeholder="Enter Your Delivery Address"></textarea>
                            </div>
                            <div class="col-12">
                                <p id="pub_pay_error" class="text-danger text-center mb-0"></p>
                            </div>
                            <div class="text-center">
                                <button type="button" class="btn1" id="btn_new_pay">Pay Now</button>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div id="pub_pay_error" class="d-none"></div>
<script>
    var totalPrice=0;
    $(document).ready(function () {
        populateProduct();        
    });
    $(document).on('change','.qtychange',function(){
        var qty=parseInt($(this).val());
        var productId=$(this).attr('productId');
        var allcart = JSON.parse(localStorage.getItem('allCart'));
        allcart.forEach((element,index) => {
            if(productId==element.productId){
                allcart[index].qty=qty
            }
        });
        localStorage.setItem('allCart',JSON.stringify(allcart));
        populateProduct()
    });
    $(document).on('click','.deleteItem',function(){
        if (confirm('Are You Sure To Delete The Item From Cart?')) {
            var productId=$(this).attr('productId');
            var allcart = JSON.parse(localStorage.getItem('allCart'));
            allcart.forEach((element,index) => {
                if(productId==element.productId){
                    allcart.splice(index, 1);
                }
            });
            localStorage.setItem('allCart',JSON.stringify(allcart));
            populateProduct()
        }
    });
    $(document).on('click', '#btn_new_pay', function() {
        var name = $('#publication_txt_name').val();
        var email = $('#publication_txt_email').val();
        var mobile = $('#publication_txt_mobile').val();
        var address = $('#publication_address').val();
        var zipcode = $('#publication_txt_zipcode').val();
        var coupon_code = $('#txt_cupon_code').val();
        if (name && email && mobile && address && zipcode) {
            prodsSent = [];
            var allcart = JSON.parse(localStorage.getItem('allCart'));
            $('.itemPrice').each(function(){
                var price = parseInt($(this).text());
                var pro_id = parseInt($(this).attr('itemid'));
                var qty = parseInt($(this).attr('itemqty'));
                var tPrice=price*qty;
                prodsSent.push({
                    "item_id": pro_id,
                    "item_type": "product",
                    "item_quantity": qty,
                    "amount": price                   
                });
            });
            var finalData = {
                "userDetails": {
                    "user_name": name,
                    "user_email": email,
                    "user_phoneNo": mobile,
                    "user_addres": address,
                    "user_pinCode": zipcode,
                    "coupon_code" : coupon_code
                },
                "prodDetails": prodsSent,
                "device_type": 'web',
                "redirect_url":"https://edudigm.in"
            }
            var formHtml="";
            postDataNoToken("https://crm.edudigm.com/api/new-initiate-order", finalData,"POST").then((data) => {
                var formdata = data.response_data;
                formHtml += '<form action="' + formdata.action + '" id="payment_form_submit" method="post">';
                formHtml += '    <input type="" id="surl" name="surl" value="' + formdata.surl + '">';
                formHtml += '    <input type="" id="furl" name="furl" value="' + formdata.furl + '">';
                formHtml += '    <input type="" id="curl" name="curl" value="' + formdata.curl + '">';
                formHtml += '    <input type="" id="key" name="key" value="' + formdata.key + '">';
                formHtml += '    <input type="" id="txnid" name="txnid" value="' + formdata.txnid + '">';
                formHtml += '    <input type="" id="amount" name="amount" value="' + formdata.amount + '">';
                formHtml += '    <input type="" id="udf1" name="udf1" value="' + formdata.udf1 + '">';
                formHtml += '    <input type="" id="udf2" name="udf2" value="' + formdata.udf2 + '">';
                formHtml += '    <input type="" id="productinfo" name="productinfo" value="' + formdata.productinfo + '">';
                formHtml += '    <input type="" id="firstname" name="firstname" value="' + formdata.firstname + '">';
                formHtml += '    <input type="" id="email" name="email" value="' + formdata.email + '">';
                formHtml += '    <input type="" id="phone" name="phone" value="' + formdata.phone + '">';
                formHtml += '    <input type="" id="hash" name="hash" value="' + formdata.hash + '">';
                formHtml += '</form>';
                $('#pub_pay_error').html(formHtml);
                document.getElementById("payment_form_submit").submit();
            });
        }else {
            toast('err',"All The Fields Are Mandatory")
        }
    });
    function populateProduct(){
        totalPrice=0;
        var allcart = JSON.parse(localStorage.getItem('allCart'));
        if(allcart && allcart.length>0 ){
            var allId=allcart.map(item => item.productId).join();
            var token="{{ csrf_token() }}";
            cartHtml="";
            postData("{{ route('getAllCart') }}", { allIds: allId },"POST",token).then((data) => {
                data.forEach(element => {
                    var qty=allcart.find(x => x.productId === element.id).qty
                    cartHtml+='<div class="col-12 position-relative">';
                    cartHtml+='    <span class="deleteItem" productId="'+element.id+'"><i class="fas fa-trash-alt"></i></span>';
                    cartHtml+='    <div class="row">';
                    cartHtml+='        <div class="col-md-3 text-center">';
                        if(element.images){
                            var splitRowObject = element.images.split(',')[0];
                            if(splitRowObject){
                                cartHtml+='            <img src="https://files.edudigm.com/Files/rcb_images/'+splitRowObject+'" class="cartImage" alt="">';
                            }else{
                                cartHtml+='            <img src="{{ asset("assets/images/soon.jpg") }}" class="w-50" alt="">';
                            }
                        }else{
                            cartHtml+='            <img src="{{ asset("assets/images/soon.jpg") }}" class="w-50" alt="">';
                        }
                        
                    cartHtml+='        </div>';
                    cartHtml+='        <div class="col-md-2"></div><div class="col-md-6">';
                    cartHtml+='            <h4 class="fw-bold text-start">'+element.name+'</h4>';
                    var itemPrice=0;
                    if(element.discount && element.discount>0){
                        itemPrice=(element.price-(element.price*element.discount/100))*qty;
                        cartHtml+='            <p class="mb-1 text-start">';
                        cartHtml+='                <span class="price me-1">';
                        cartHtml+='                    <s>₹'+element.price+'</s>';
                        cartHtml+='                </span>';
                        cartHtml+='                <span class="text-success me-1">('+element.discount+'% off)</span>';
                        cartHtml+='                <span class="text-uppercase prices h4">';
                        cartHtml+='                    ₹ '+element.price*element.discount/100+' X '+qty+' = ₹';
                        cartHtml+=                    '<span class="itemPrice" itemQty="'+qty+'" itemId="'+element.id+'">'+itemPrice+'</span>';
                        cartHtml+='                </span>';
                        cartHtml+='            </p>';
                    }else{
                        itemPrice=(element.price)*qty;
                        cartHtml+='            <p class="mb-1 text-start">';
                        cartHtml+='                <span class="text-uppercase prices h4">';
                        cartHtml+='                    ₹'+element.price+' X '+qty+' = ₹'+'<span class="itemPrice" itemQty="'+qty+'" itemId="'+element.id+'">'+itemPrice+'</span>';
                        cartHtml+='                </span>';
                        cartHtml+='            </p>';
                    }
                    totalPrice=totalPrice+itemPrice;
                    cartHtml+='             <p class="text-start">Quantity:';
                    cartHtml+='                 <select class="qtychange" productId="'+element.id+'">';
                        if(qty==1){
                            cartHtml+='<option value="1" selected>1</option>';
                        }else{
                            cartHtml+='<option value="1">1</option>';
                        }
                        if(qty==2){
                            cartHtml+='<option value="2" selected>2</option>';
                        }else{
                            cartHtml+='<option value="2">2</option>';
                        }
                        if(qty==3){
                            cartHtml+='<option value="3" selected>3</option>';
                        }else{
                            cartHtml+='<option value="3">3</option>';
                        }
                        if(qty==4){
                            cartHtml+='<option value="4" selected>4</option>';
                        }else{
                            cartHtml+='<option value="4">4</option>';
                        }
                        if(qty==5){
                            cartHtml+='<option value="5" selected>5</option>';
                        }else{
                            cartHtml+='<option value="5">5</option>';
                        }
                    cartHtml+='                 </select>';
                    cartHtml+='             </p>';
                    cartHtml+='        </div>';
                    cartHtml+='    </div>';
                    cartHtml+='    </div>';
                    cartHtml+='<hr/>';
                });
                $('#cartData').html(cartHtml);
                $('#totalPrice').html('₹'+totalPrice);
            });
        }else{
            $('#cartData').html('<div class="col-12"><h1 class="text-center">No Items In Cart</h1></div>');
            $('#totalprice').hide();
        }
    }
</script>