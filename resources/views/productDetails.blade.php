<title>Home</title>
@include('include.header')
<link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<style>
    .owl-carousel button.owl-dot {
        background-color: #798083;
        width: 10px;
        height: 10px;
        border-radius: 50px;
        margin: 6px 2px 0;
    }

    .owl-carousel button.owl-dot.active {
        background-color: var(--col1) !important;
    }

    .owl-dots {
        text-align: center;
    }

    .owl-carousel .owl-nav {
        display: none;
    }

    .SDas {
        margin: 0 auto;
    }

    .fa-rupee-sign {
        font-size: 20px;
    }

    .howCard img {
        max-width: 100px;
    }
    .offers img {
        max-width: 100px;
    }

    .whatcard {
        box-shadow: 0px 0px 5px 0px rgb(0 0 0 / 12%);
    }
    .howCard {
        background-color: #F2F0F4;
    }
    .offerings:hover{
        border: 1px solid var(--col1);
    }
</style>
<div class="content">
    <div class="top_menu_header">
        @include('include.top_menu')
    </div>
</div>
<section class="mt-4 pt-5">
    <iframe src="https://www.youtube.com/embed/{{$product[0]->video}}" title="YouTube video player" frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
        allowfullscreen="" style="height: 300px; width: 100%;" id="youVideo"></iframe>
</section>
<div class="container mt-5 pt-5">
    {{-- Why --}}
    <div class="row">
        <div class="col-md-6 mt-3">
            <div class="product_description ms-">
                <h3 class="fw-bold product_name product_id">{{$product[0]->name}}</h3>
                @if ($product[0]->discount && $product[0]->discount > 0)
                <p class="mb-1">
                    <span class="price me-1">
                        <s><i class="fas fa-rupee-sign"></i>{{$product[0]->price}}</s>
                    </span>
                    <span class="text-success me-1">({{$product[0]->discount}}% off)</span>
                    <span class="text-uppercase prices h4">
                        <i class="fas fa-rupee-sign"></i> {{
                        $product[0]->price-($product[0]->price*$product[0]->discount/100) }}
                    </span>
                </p>
                @else
                <p class="mb-1">
                    <span class="text-uppercase prices h4">
                        <i class="fas fa-rupee-sign"></i>{{$product[0]->price}}
                    </span>
                </p>
                @endif
                <p>
                    {!! $product[0]->why ? $product[0]->why : "" !!}
                </p>
            </div>

            <div class="pro_action_btn mt-3">
                <span class="btn1 me-4" onclick="addToCart(<?php echo $_GET['id'] ?>)">Add to Cart</span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="owl-carousel owl-theme text-center">
                <?php
                    $array = explode(",", $product[0]->images);
                ?>
                @foreach($array as $item)
                @if($item)
                    <?php $item=str_replace('rcb_images/', '', $item) ?>
                    <img src="https://files.edudigm.com/Files/rcb_images/{{$item}}" alt="">
                @else
                <img src="{{ asset('assets/images/soon.jpg') }}" alt="" class="w-50 SDas">
                @endif
                @endforeach

            </div>
        </div>
    </div>
    {{-- Why --}}
    {{-- How --}}
    <div class="bg py-5" style="background-image: url('{{ asset('assets/images/Ellipse423.png') }}');background-size: contain; background-position: center; background-repeat: no-repeat;">
        <div class="row">
            <h3 class="text-center">
                <h1 class="text-center fw-bold">The <span class="col1"> {{$product[0]->name}}</span> offers a dynamic platform for students from</h1>                 
            </h3>
        </div>
        <div class="row my-4 justify-content-center align-items-center">
            @foreach($deliverables as $deliverable)  
                @if(($deliverable->type)=="offer")                      
                    <div class="col-md-3">
                        <div class="howCard text-center py-4">
                            <img src="https://files.edudigm.com/Files/{{$deliverable->images}}" alt="">
                            <p>{{$deliverable->name}}</p>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
        <div class="row">
            <div class="col-12 my-4">
                <p id="howtext">{!!$product[0]->how!!}</p>
            </div>
        </div>
    </div>
    {{-- How --}}
    {{-- What --}}
    <div class="row">
        <div class="col-12">
            <h1 class="text-center fw-bold"><span class="col1"> {{$product[0]->name}}</span> Offers</h1>
            <h5 class="text-center fw-bold">Why you should enroll?</h5>
        </div>
    </div>
    <div class="row justify-content-center align-items-center my-5">
        @foreach($deliverables as $deliverable)  
            @if(($deliverable->type)=="enroll")   
                <div class="col-md-3">
                    <div class="whatcard text-center py-4">
                        <img src="https://files.edudigm.com/Files/{{$deliverable->images}}" alt="">
                        <p>{{$deliverable->name}}</p>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
    <div class="row">
        <p>{!!$product[0]->what!!}</p>
    </div>
    {{-- What --}}
</div>
<div class="container">
    <div class="row">
        <div class="col-12">
            <img src="" class="img-fluid" id="whatImage" alt="">
        </div>
    </div>
</div>
<h1 class="text-center fw-bold col1 mt-5">Impact</h1>
<div class="impactbg py-5" style="background-image: url({{ asset('assets/images/stats_bg.png') }});background-size: cover; background-position: center; background-repeat: no-repeat;">
    <div class="container">
        <div class="row  justify-content-center align-items-center" id="impactCardHtml">
            @foreach($deliverables as $deliverable)  
                @if(($deliverable->type)=="impact")   
                    <div class="col-md-3">
                        <img src="https://files.edudigm.com/Files/{{$deliverable->images}}" alt="" class="img-fluid">
                        <p class="text-light text-center mt-4">
                            {{$deliverable->name}}
                        </p>
                    </div>
                @endif
            @endforeach
        </div>
        <div class="row">
            <div class="col-12">
                <p class="text-light pt-5 fw-bold">
                    {!!$product[0]->impact!!}
                </p>
            </div>
        </div>
    </div>
</div>
<h1 class="text-center mt-5 fw-bold">Our <span class="col1">Offerings</span></h1>
<div class="container">
    <div class="row justify-content-center align-items-center">
        @foreach($deliverables as $deliverable)  
            @if(($deliverable->type)=="deliverable")   
                <div class="col-md-3">
                    <p class="offerings card px-4 py-4 fw-bold">
                        {{$deliverable->name}}
                    </p>
                </div>
            @endif
        @endforeach
    </div>
</div>
<h1 class="text-center mt-5 fw-bold">Next <span class="col1">Steps</span></h1>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-12">
            {!!$product[0]->next_steps!!}
        </div>
    </div>
</div>
<section class="mt-md-5">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <h1 class="text-center fw-bold"><span class="col1"> FAQs</span></h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <div class="accordion" id="accordionFlushExample">                    
                    @foreach($faqs as $faq)                        
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="heading{{$faq->id}}">
                                <span class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">
                                    {{$faq->question}}
                                </span>
                            </h2>
                            <div id="collapse{{$faq->id}}" class="accordion-collapse collapse" aria-labelledby="heading{{$faq->id}}"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    {{$faq->answer}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container mt-5">
    <div class="row">
        <div class="col-12">
            @include('include.commonSection')
        </div>
    </div>
</div>
<div class="bottom_footer_menu">
    @include('include.bottom_footer')
</div>
<script src="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js"></script>
@include('include.footer')
@include('include.operationCart')
<script>
     $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 30,
        dots: true,
        nav: true,
        items: 1,
    })
</script>
{{-- <script>
   
    var resData={"id":72,"video":"https://www.youtube.com/embed/khlbomFl6Gc","name":"Revision Chart Books: Science & Mathematics","discount":10,"price":700,"image":"rs1.png,rs2.png","whytext":"In a world brimming with information and educational resources, the challenge for students is not just in learning new concepts but in retaining, revising, and recalling them effectively. Edudigm addresses this challenge head-on with its innovative Revision Chart Books (RCBs). These books are designed to help students visualize and connect various concepts within a chapter, making revision not just a task but a journey of discovery. With RCBs, students, parents, and educators have a powerful tool to enhance learning outcomes, making complex topics accessible and memorable.","howCard":[{"image":"rs1.png","text":" classes  3 to 5"},{"image":"rs2.png","text":" classes  6 to 58"},{"image":"rs3.png","text":" classes  9 to 12"}],"howText":"RCBs stand out by combining important concepts and topics with colorful illustrations and diagrams, presenting a complete picture of the subject matter in a way that is engaging and easy to understand. This unique approach aids in review, revision, retention, and recall, allowing students to grasp the connections between different concepts seamlessly. Available for Science (Physics, Chemistry, Biology, Environmental Science) and Mathematics for classes 10, 11, & 12, RCBs are crafted to cater to a wide range of learners, making them an indispensable part of any student's study regimen.","whatCard":[{"image":"rs1.png","text":"national-level competition"},{"image":"rs2.png","text":"national-level competition"},{"image":"rs3.png","text":"national-level competition"}],"whatText":"<ul> <li>The Eureka STEM quiz encompasses a broad spectrum of topics within the STEM fields.</li><li>Providing a comprehensive platform for students to test their knowledge.</li><li>This national-level competition is structured in a way that makes learning an enjoyable and rewarding experience.</li><li>Winners and participants are recognized for their talents and encouraged through prizes and certificates, making Eureka an anticipated event in the academic calendar.</li></ul>","whatImage":"rs1.png","impact":[{"image":"rs1.png","text":"The Eureka STEM Quiz offers a dynamic platform for students from "},{"image":"rs2.png","text":"The Eureka STEM Quiz offers a dynamic platform for students from "},{"image":"rs1.png","text":"The Eureka STEM Quiz offers a dynamic platform for students from "},{"image":"rs2.png","text":"The Eureka STEM Quiz offers a dynamic platform for students from "},{"image":"rs1.png","text":"The Eureka STEM Quiz offers a dynamic platform for students from "},{"image":"rs2.png","text":"The Eureka STEM Quiz offers a dynamic platform for students from "}],"impactText":"The impact of RCBs is profound, with students reporting improved performance in exams, enhanced understanding of subjects, and a more enjoyable revision experience. Educators and parents alike have praised the effectiveness of RCBs in making revision an engaging process, leading to better outcomes and a deeper appreciation for the subjects studied. Edudigm's commitment to innovative educational tools is evident in the success of the RCBs, which have become a favorite among students striving for academic excellence.","offerings":["The impact of the Eureka STEAM Quiz extends beyond the competition itself. ","Participants often report a heightened interest in STEM subjects and an improved ability to work under pressure.","Schools and educators have noted an increase in collaborative learning and engagement among students, attributing this change to the excitement and challenge presented by the quiz.","Eureka has successfully managed to create a buzz around STEAM education, encouraging students to pursue their interests further."],"nextSteps":"<ul><li>TheEurekaSTEMquizencompassesabroadspectrumoftopicswithintheSTEMfields.</li><li>Providingacomprehensiveplatformforstudentstotesttheirknowledge.</li><li>Thisnational-levelcompetitionisstructuredinawaythatmakeslearninganenjoyableandrewardingexperience.</li><li>Winnersandparticipantsarerecognizedfortheirtalentsandencouragedthroughprizesandcertificates,makingEurekaananticipatedeventintheacademiccalendar.</li></ul>",};
    // console.log(resData);
    $('#whyText').text(resData.whytext);
    $('#youVideo').attr('src',resData.video);
    $('.product_name').text(resData.name);
    var priceHtml="";
    if(resData.discount && resData.discount>0){
        priceHtml+=`<p class="mb-1" id="price">`;
        priceHtml+=`    <span class="price me-1">`;
        priceHtml+=`        <s><i class="fas fa-rupee-sign"></i>`+resData.price+`</s>`;
        priceHtml+=`    </span>`;
        priceHtml+=`    <span class="text-success me-1">(`+resData.discount+`% off)</span>`;
        priceHtml+=`    <span class="text-uppercase prices h4">`;
        priceHtml+=`        <i class="fas fa-rupee-sign"></i>`;
        priceHtml+=        resData.price-(resData.price*resData.discount/100);
        priceHtml+=`    </span>`;
        priceHtml+=`</p>`;
    }else{
        priceHtml+=`<span class="text-uppercase prices h4">`;
        priceHtml+=`    <i class="fas fa-rupee-sign"></i>`+resData.price+``;
        priceHtml+=`</span>`;
    }
    $('#price').html(priceHtml);
    var howCardHtml="";
    resData.howCard.forEach(element => {
        howCardHtml+=`<div class="col-md-3">`;
        howCardHtml+=`    <div class="howCard text-center py-4">`;
        howCardHtml+=`        <img src="{{ asset('assets/images/image163.png') }}" alt="">`;
        howCardHtml+=`        <img src="https://files.edudigm.com/Files/rcb_images/`+element.image+`" alt="">`;
        howCardHtml+=`        <p>`+element.text+`</p>`;
        howCardHtml+=`    </div>`;
        howCardHtml+=`</div>`;
    });
    $('#howhtml').html(howCardHtml);
    $('#howtext').html(resData.howText);
    var offerCardHtml="";
    resData.whatCard.forEach(element => {
        offerCardHtml+=`<div class="col-md-3">`;
        offerCardHtml+=`    <div class="howCard text-center py-4">`;
        offerCardHtml+=`        <img src="{{ asset('assets/images/Icon.png') }}" alt="">`;
        offerCardHtml+=`        <img src="https://files.edudigm.com/Files/rcb_images/`+element.image+`" alt="">`;
        offerCardHtml+=`        <p>`+element.text+`</p>`;
        offerCardHtml+=`    </div>`;
        offerCardHtml+=`</div>`;
    });
    $('#offerCard').html(offerCardHtml);
    $('#offerHtml').html(resData.whatText);
    $('#whatImage').attr('src','{{ asset('assets/images/2480554.png') }}');

    var impactCardHtml="";
    resData.impact.forEach(element => {
        impactCardHtml+='<div class="col-md-3">';
        impactCardHtml+='    <img src="{{ asset('assets/images/2480561.png') }}" alt="" class="img-fluid">';
        impactCardHtml+='    <p class="text-light text-center mt-4">';
        impactCardHtml+='        The Eureka STEM Quiz offers a dynamic platform for students from ';
        impactCardHtml+='    </p>';
        impactCardHtml+='</div>';
    });
    
    $('#impactCardHtml').html(impactCardHtml);
    $('#impacttext').html(resData.impactText);
    var offeringCardHtml="";
    resData.offerings.forEach(element => {
        offeringCardHtml+=`<div class="col-md-3">`;
        offeringCardHtml+=`    <p class="offerings card px-4 py-4 fw-bold">`;
        offeringCardHtml+=element;
        offeringCardHtml+=`    </p>`;
        offeringCardHtml+=`</div>`;
    });
    
    $('#offeringhtml').html(offeringCardHtml);
    $('#nextSteps').html(resData.nextSteps);
</script> --}}