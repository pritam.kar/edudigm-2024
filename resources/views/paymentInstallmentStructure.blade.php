@include('include.header')
<div class="content">
    <div class="top_menu_header">
        @include('include.top_menu')
    </div>
</div>
<div class="container">
    <p id="formDivPub" class="d-none"></p>
        <div class="loader">
            <img src="https://www.edudigm.in/assets/images/loading.gif" alt="">
        </div>
        <div class="container-fluid If_data" style="display:none;">
            <div class="row">
                <div class="" style="background-color: #fff;box-shadow: rgb(0 0 0 / 16%) 12px 3px 24px;">
                    <div class="col-md-12">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-md-3 text-center py-3">
                                    <img src="http://www.edudigm.in/eduassets/images/logo.png" class="mt-2 w-75" alt="">
                                </div>
                                <div class="col-md-3">

                                </div>
                                <div class="col-md-6 py-3">
                                    <h1 class="text-center text-uppercase mb-0" style="color:#fd5f00;">Installment Details</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container my-3 If_data" style="display:none;">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <h3 class="color_blue text-center">Installment Details Of <b><span class="h3 color_blue" id="name"></span></b></h3>
                </div>
                <div class="col-md-12 mt-3">
                    <div class="row">
                        <div class="col-md-4">
                            <h6 class="mb-0 text-center color_blue" id="class"><b>Class:</b> </h6>
                        </div>
                        <div class="col-md-4">
                            <h6 class="mb-0 text-center color_blue" id="phone"><b>Phone:</b> </h6>
                        </div>
                        <div class="col-md-4">
                            <h6 class="mb-0 text-center color_blue" id="email"><b>Email:</b> </h6>
                        </div>
                        <div class="col-md-6">
                            <h6 class="mb-0 mt-3 color_blue text-center" id="address"><b>Address:</b> </h6>
                        </div>
                        <div class="col-md-6">
                            <h6 class="mb-0 mt-3 color_blue text-center" id="school" style="display: none;"><b>School:</b> </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container If_data" style="display:none;">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <table class="table align-middle">
                        <thead clas="table-dark">
                            <tr>
                                <th>Installment</th>
                                <!-- <th>Batch</th> -->
                                <th>Due Date</th>
                                <th>Payment Made on</th>
                                <th>Mode Of Payment</th>
                                <th>Amount</th>
                                <th>Transaction Id</th>
                                <th>Invoice</th>
                            </tr>
                        </thead>
                        <tbody id="payment_details_table"></tbody>
                    </table>
                </div>
                <div class="col-12 mt-2 mb-5">
                    <div class="row">
                        <div class="col-12">
                            <h6 class="text-center">Thank you for choosing Edudigm</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="emailmodal" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Email Id</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <label for="txt_email" class="ps-2">Please Provde Your Mail Id</label>
                    <input type="email" name="" id="txt_email" class="email form-control" placeholder="Email Id">                
                </div>
                <small class="text-success ps-4">We will send payment details here</small>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary pay_now_modal">Pay Now</button>
                </div>
                </div>
            </div>
        </div>
</div>
<div class="bottom_footer_menu">
    @include('include.bottom_footer')
</div>
@include('include.footer')
<script>
    var tablehtml = "";
    var student_id = window.location.pathname.substr(window.location.pathname.indexOf("/") + 9);
    var e = {
        "edudigm_id": student_id
    };
    var prodsSent = [];
    var finalData = [];
    var allresponse = [];
    response = getresponsedata(e);
    if (response.response_code == 200) {
        allresponse = response;
        $('#name').append(response.response_data.details.name);
        $('#class').append(response.response_data.details.class);
        $('#phone').append(response.response_data.details.phone);
        if(!response.response_data.details.email){
            $('#email').hide();
        }
        $('#email').append(response.response_data.details.email);
        if(!response.response_data.details.address){
            $('#address').hide();
        }
        $('#address').append(response.response_data.details.address);
        if (response.response_data.details.school) {
            $('#school').show().append(response.response_data.details.school);
        }
        if(response.response_data.installment.length>0){
            $.each(response.response_data.installment, function(index, value) {
                var installmentNumber = index + 1;
                tablehtml += '<tr>';
                tablehtml += '<td>' + value.fees_type + '</td>';
                // if (value.batch == "AR") {
                //     tablehtml += '<td>Arjuna</td>';
                // } else if (value.batch == "EK") {
                //     tablehtml += '<td>Eklavya</td>';
                // }else{
                //     tablehtml += '<td></td>';
                // }
                if (value.due_date) {
                    tablehtml += '<td>' + value.due_date + '</td>';
                } else {
                    tablehtml += '<td></td>';
                }
                if (value.submitted_date) {
                    tablehtml += '<td>' + value.submitted_date + '</td>';
                } else {
                    tablehtml += '<td></td>';
                }
                if (value.mode) {
                    tablehtml += '<td>' + value.mode + '</td>';
                } else {
                    tablehtml += '<td></td>';
                }
                if (value.amount) {
                    tablehtml += '<td>' + value.amount + '</td>';
                } else {
                    tablehtml += '<td></td>';
                }
                if (value.transaction_id) {
                    tablehtml += '<td>' + value.transaction_id + '</td>';
                    tablehtml += '<td><a href="/invoice?transaction_id=' + value.transaction_id + '" class="text-primary"><u>Invoice</u></a></td>';
                    // tablehtml += '<td><a href="https://www.edudigm.in/invoice/' + value.transaction_id + '">Invoice</a></td>';
                } else {
                    tablehtml += '<td></td>';
                    if (!value.submitted_date) {
                        tablehtml += '<td><p class="btn1 mb-0 pay_now" student_amount="' + value.amount + '" fee_submission_id="' + value.fee_submission_id + '">Pay Now</p></td>';
                    }
                }
                tablehtml += '</tr>';
            });
            $('#payment_details_table').html(tablehtml);
        }else{
            $('body').html('<h1 class="text-center text-danger my-5 py-5">No Data Found</h1>');
        }
        
        $('.If_data').show();
        $('.loader').hide();
    } else {
        alert("Please Enter A Valid Edudigm Id");
        $('.loader').hide();
        $('body').html('<h1 class="text-center text-danger my-5 py-5">No Data Found</h1>');
    }
    $(document).on('click', '.pay_now', function() {
        var fee_submission_id = $(this).attr("fee_submission_id");
        var student_amount = $(this).attr("student_amount");
        var email = allresponse.response_data.details.email;
        $('.pay_now_modal').attr("fee_submission_id",fee_submission_id);
        $('.pay_now_modal').attr("student_amount",student_amount);
        paynow(email,fee_submission_id,student_amount); 
    });
    $(document).on('click', '.pay_now_modal', function() {
        var email = $('#txt_email').val();
        var fee_submission_id = $(this).attr("fee_submission_id");
        var student_amount = $(this).attr("student_amount");
        if(email ==""){
            $('#txt_email').css('border-bottom-color', '#ff4343');
            $('#txt_email').parent().find('.error_msg').remove();
            $('#txt_email').parent().append('<span class="error_msg" style="color:red;">Please Enter A Valid Email Id</span>')
        }else{
            paynow(email,fee_submission_id,student_amount); 
        }
        
    });
    function paynow(email,fee_submission_id,student_amount){
        var prodsSent=[];
        var finalData=[];
        var name = allresponse.response_data.details.name;            
        var phone = allresponse.response_data.details.phone;
        var addres = allresponse.response_data.details.address;
        if(!addres){
            addres="No Address Specified";
        }
        if(email){
            prodsSent.push({
                "item_id": fee_submission_id,
                "item_type": "installment",
                "item_quantity": "1",
                "amount": student_amount,
            });
            finalData = {
                "userDetails": {
                    "user_name": name,
                    "user_email": email,
                    "user_phoneNo": phone,
                    "user_pinCode": "000000",
                    "user_addres": addres
                },
                "prodDetails": prodsSent,
                "only_reg": '1'
            }
            var initiate_order_response = Cart_getSubtotal(finalData)
            var response_code = initiate_order_response.response_code;
            if (response_code == 200) {
                formHtml="";
                var formdata = initiate_order_response.response_data;
                formHtml += '<form action="' + formdata.action + '" id="payment_form_submit" method="post">';
                formHtml += '    <input type="" id="surl" name="surl" value="' + formdata.surl + '">';
                formHtml += '    <input type="" id="furl" name="furl" value="' + formdata.furl + '">';
                formHtml += '    <input type="" id="curl" name="curl" value="' + formdata.curl + '">';
                formHtml += '    <input type="" id="key" name="key" value="' + formdata.key + '">';
                formHtml += '    <input type="" id="txnid" name="txnid" value="' + formdata.txnid + '">';
                formHtml += '    <input type="" id="amount" name="amount" value="' + formdata.amount + '">';
                formHtml += '    <input type="" id="udf1" name="udf1" value="' + formdata.udf1 + '">';
                formHtml += '    <input type="" id="udf2" name="udf2" value="' + formdata.udf2 + '">';
                formHtml += '    <input type="" id="productinfo" name="productinfo" value="' + formdata.productinfo + '">';
                formHtml += '    <input type="" id="firstname" name="firstname" value="' + formdata.firstname + '">';
                formHtml += '    <input type="" id="email" name="email" value="' + formdata.email + '">';
                formHtml += '    <input type="" id="phone" name="phone" value="' + formdata.phone + '">';
                formHtml += '    <input type="" id="hash" name="hash" value="' + formdata.hash + '">';
                formHtml += '</form>';
                $('#formDivPub').html(formHtml);
                document.getElementById("payment_form_submit").submit();
            } else {
                alert(response.response_msg);
            }
        }else{
            $('#emailmodal').modal('show');
        }
    }
    function getresponsedata(e) {
        return $.parseJSON(
            $.ajax({
                type: "POST",
                url: "https://crm.edudigm.com/api/website/get-student-details",
                data: e,
                dataType: "json",
                async: !1
            }).responseText
        )
    }
    function Cart_getSubtotal(e) {
        return $.parseJSON($.ajax({
            type: "POST",
            url: "https://crm.edudigm.com/api/new-initiate-order",
            data: e,
            dataType: "json",
            async: !1
        }).responseText)
    }
    function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
        return false;
    };
    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test($email);
    }
    $('.email').on('input', function() {
        var value = $(this).val();
        if (value) {
            if (!validateEmail(value)) {
                $(this).css('border-bottom-color', '#ff4343');
                $(this).parent().find('.error_msg').remove();
                $(this).parent().append('<span class="error_msg" style="color:red;">Please Enter A Valid Email Id</span>')
            } else {
                $(this).css('border-bottom-color', '#008000');
                $(this).parent().find('.error_msg').remove();
            }
        } else {
            $(this).css('border-bottom-color', '#ff4343');
        }
    });
</script>