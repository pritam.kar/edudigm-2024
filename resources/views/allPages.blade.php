<title>{{$allData[0]->title}}</title>
<style>
    .whysection {
        background-image: url(/assets/images/programDetailsSideImg.png), url(/assets/images/programDetailsWhyBg.png);
        background-position: right center, center center;
        background-repeat: no-repeat, repeat;
        background-size: 40%, 100%;
    }
    .steps .counter{
        display: inline-block;
        width: 30px;
        height: 30px;
        border-radius: 50%;
        color: #fff;
        line-height: 35px;
    }
    .steps:nth-child(1) .counter,.steps:nth-child(4) .counter,.steps:nth-child(7) .counter{
        background-color: #F64B4B;
    }
    .steps:nth-child(2) .counter,.steps:nth-child(5) .counter,.steps:nth-child(8) .counter{
        background-color: #68D585;
    }
    .steps:nth-child(3) .counter,.steps:nth-child(6) .counter,.steps:nth-child(9) .counter{
        background-color: #473BF0;
    }
    .offerings:hover {
        border: 1px solid var(--col1);
    }
    .whatImg{
        width: 50px;
        margin: 0 auto;
    }
</style>
@include('include.header')
<div class="content">
    <div class="top_menu_header">
        @include('include.top_menu')
    </div>
</div>
<?php $alldata = json_decode($allData[0]->content) ?>
<section class="mt-4 pt-5">
    <iframe src="https://www.youtube.com/embed/{{$alldata->page_video}}" title="YouTube video player" frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
        allowfullscreen="" style="height: 300px; width: 100%;"></iframe>
</section>
<section class="mt-5 whysection">
    <div class="container-fluid">
        <div class="ps-md-5">
            <div class="row">
                <div class="col-md-4">
                    {{-- <p class="text-center mt-md-5">
                        <span class="btn2">Class VIII</span>
                    </p> --}}
                    <h1 class=" mt-md-5">{!!$alldata->why_title!!}</h1>
                    <p class="mt-md-5">
                        {!!$alldata->why_desc!!}
                    </p>

                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid pb-5">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center">
                    &nbsp;
                </h1>
                <p class="text-center">
                    &nbsp;
                </p>
            </div>
        </div>
    </div>
</section>
<section class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center">
                    {!!$alldata->how_heading!!}
                </h1>
                <p class="text-center">
                    {!!$alldata->how_title!!}
                </p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-4 text-center">
                <div class="imageBg px-4 py-5">
                    <img src="https://files.edudigm.com/Files/{{$alldata->how_images[0]}}" class="w-75" alt="">
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="imageBg px-4 py-5">
                    <img src="https://files.edudigm.com/Files/{{$alldata->how_images[1]}}" class="w-75" alt="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p class="text-center">
                    {!!$alldata->how_desc!!}
                </p>
            </div>
        </div>
    </div>
</section>
<section class="mt-md-5">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <h1 class="text-center fw-bold">{!!$alldata->what_heading!!}</h1>
            </div>
            <div class="col-12">
                <h4 class="text-center fw-bold">{!!$alldata->what_title!!}</h4>
            </div>
        </div>
        <div class="row  justify-content-center align-items-center">
            @foreach($alldata->why_features as $i=>$product)
                <div class="col-md-3 mb-4">
                    <div class="offerings text-center card px-4 py-4 fw-bold">
                        <img src="https://files.edudigm.com/Files/{{$alldata->why_features_files[$i]}}" class="whatImg" alt="">
                        <p class="pt-3">{{$alldata->why_features[$i]}}</p>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-12">
                <p class="text-center">{!!$alldata->what_des!!}</p>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-12">
            <h1 class="text-center fw-bold"><span class="col1"> Impact</span></h1>
        </div>
    </div>
    <div class="row justify-content-center align-items-center">
        <div class="col-md-6">
            <img src="https://files.edudigm.com/Files/{{$alldata->impact_image}}" class="img-fluid" alt="">
        </div>
    </div>
    <div class="row justify-content-center align-items-center">
        <div class="col-md-6 mt-4">
            <h4 class="text-center">
                {!!$alldata->impact_text!!}
            </h4>
        </div>
    </div>

</div>
<section class="mt-md-5">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <h1 class="text-center fw-bold">{!!$alldata->next_steps_heading!!}</h1>
            </div>
        </div>
        <div class="row justify-content-center align-items-center">
            <div class="col-md-12 mt-4">
                <h4 class="text-center">
                    {!!$alldata->next_steps_desc!!}
                </h4>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 text-center">
                <h1 class="fw-bold"><span class="col1">{!!$alldata->how_to_enroll_heading!!}</span></h1>
                {!!$alldata->how_to_enroll_desc!!}
            </div>
        </div>
        <div class="row justify-content-center align-items-center">
            @foreach($alldata->enroll_heading as $i=>$product)
            <div class="col-md-4 mt-4 steps">
                <div class="card rouned-3 py-2 px-4">
                    <p class="text-center">
                        <span class="counter">{{$i+1}}</span>
                    </p>
                    <p class="text-center fw-bold">
                        {{$alldata->enroll_heading[$i]}}
                    </p>
                    <p>
                        {{$alldata->enroll_paragraph[$i]}}
                    </p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@include('include.commonSection')
<div class="bottom_footer_menu">
    @include('include.bottom_footer')
</div>
@include('include.footer')