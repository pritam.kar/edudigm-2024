@include('include.header')

<style>
    @media print {
        body {
            print-color-adjust: exact;
            -webkit-print-color-adjust: exact;
        }
    }   
    *{
        font-family: "lato",sans-serif;
    }
    .printMe {
        position: fixed;
        top: 50%;
        transform: translate(-1px, -50%);
        background-color: #FD5F00;
        border: 1px solid #fff;
        padding: 5px 20px;
        color: #fff;
        cursor: pointer;
        right: 0;
    }
    .pageDiv {
        width: 885px;
        height: 1250px;
        /* border: 2px solid #FF6100; */
        border-radius: 25px;
        padding: 15px;
        position: relative;
        display: inline-block;
        margin: 15px;
    }
    .w-40{
        width: 40%;
    }
    .bulbBg{
        background-color: #FD5F00;
        display: inline-block;
    }
    td{
        text-align: center; 
        vertical-align: middle;
    }
    .footerRight{
        background-color: #FD5F00;
        border-radius: 50px 0px 0px 0px;
        padding: 5px 0;
    }
    footer{
        position: absolute;
        width: 100%;
        bottom: 0;
    }
</style>
<div class="pageDiv pt-0" id="vardiv">
    <header>
        <div class="row align-items-center">
            <div class="col-6">
                <div class="bulbBg">
                    <img src="{{ asset('assets/images/edu_logo_white.png') }}" style="width:200px;margin: 25px 25px 10px 25px;"
                        alt="">
                </div>
                <img src="eduassets/images/logo_orange_text.png" style="width:200px;margin-left:15px;" alt="">
            </div>
            <div class="col-6 text-end">
                <span style="color:#B5B5B5;font-weight:bold;border-bottom: 2px solid #FD5F00;" class="h1">Tax
                    Invoice</span>
            </div>
        </div>
    </header>
    <div class="row justify-content-center mt-5 align-items-center">
        <div class="col-8">
            <p class="mb-0"><b>Edudigm Education Services Pvt. Ltd.</b></p>
            <p class="mb-0">Ambuja Ecostation Building, 12th floor, Suite 1202, BP 64, Sector 5, Saltlake, Kolkata-
                700091</p>
            <p class="mb-0">GSTIN/UIN: 19AACCE8015C1ZY</p>
            <p class="mb-0">State Name: West Bengal, Code: 19</p>
        </div>
        <div class="col-4 text-end">
            <p class="mb-0 fw-bold">Invoice No: <span class="Invoice_Id"></span></p>
            <p class="mb-0 fw-bold">Payment date: <span class="Payment_date"></span></p>
        </div>
    </div>
    <div class="row justify-content-center mt-3 align-items-center">
        <div class="col-12">
            <div style="background: #F5F5F5;border: 1px solid #707070;" class="py-3">
                <p class="mb-0 ms-3"><b>Buyer:</b><span class="buyer_name"></span></p>
                <p class="mb-0 ms-3"><b>Address:</b> <span class="buyer_address"></span></p>
                <p class="mb-0 ms-3"><b>Pin code:</b> <span class="buyer_pincode"></span></p>
                <p class="mb-0 ms-3"><b>COE:</b> <span class="buyer_coe"></span></p>
            </div>
        </div>
    </div>
    <table class="table table-bordered mt-3">
        <tbody id="var_invoice_html">

        </tbody>
    </table>
    <table class="table table-bordered mt-3">
        <tbody id="var_invoices_html">
        </tbody>
    </table>
    <p><b> Rounding Off: </b><b class="var_txt_round"></b></p>
    <p><b> Tax Amount (in words): Indian Rupees </b><b class="var_tax_inwords"></b></p>
    <div class="row justify-content-center mt-3 align-items-center">
        <div class="col-8">
            <p style="max-width:400px;"><b>Declaration:</b><br /> We declare that this shows the actual price of the
                goods described and that all particulars are true and correct.</p>
        </div>
        <div class="col-4 text-center" style="background-color: #d3d3d38c;border: solid 1px darkgray;height: 165px;">
            <p class="mb-0 fw-bold text-center"
                style="border-bottom: solid 1px darkgray;margin: 0 -13px;font-size: 20px; padding:10px 0;">Payment Mode:
                <span class="lbl_payment_mode"></span></p>
            <p style="font-size: 13px;text-align: center;line-height: 30px;"> <b>For Edudigm Education Services Pvt.
                    Ltd</b></p>
            <img src="eduassets/images/edu_stamp.png" style="width: 165px;" alt="">
        </div>
    </div>
    <footer>
        <div class="row">
            <div class="col-3">
                <p style="font-size: 12px;margin-bottom: 0;">This is a computer generated invoice</p>
            </div>
            <div class="col-9">
                <div class="footerRight">
                    <div class="row">
                        <div class="col-4">
                            <p class="mb-0 text-center text-light" style="font-size: 15px;"><i
                                    class="fas fa-phone-alt"></i> +919230734848</p>
                        </div>
                        <div class="col-4">
                            <p class="mb-0 text-center text-light" style="font-size: 15px;"><i
                                    class="fas fa-envelope"></i> contact@edudigm.in</p>
                        </div>
                        <div class="col-4">
                            <p class="mb-0 text-center text-light" style="font-size: 15px;"><i class="fas fa-globe"></i>
                                edudigm.in</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<div class="pageDiv pt-0" id="fixdiv">
    <header>
        <div class="row align-items-center">
            <div class="col-6">
                <div class="bulbBg">
                    <img src="eduassets/images/logo_white_bulb.png" style="width:70px;margin: 25px 25px 10px 25px;"
                        alt="">
                </div>
                <img src="eduassets/images/logo_orange_text.png" style="width:200px;margin-left:15px;" alt="">
            </div>
            <div class="col-6 text-end">
                <span style="color:#B5B5B5;font-weight:bold;border-bottom: 2px solid #FD5F00;" class="h1">Tax
                    Invoice</span>
            </div>
        </div>
    </header>
    <div class="row justify-content-center mt-5 align-items-center">
        <div class="col-8">
            <p class="mb-0"><b>Edudigm Education Services Pvt. Ltd.</b></p>
            <p class="mb-0">Ambuja Ecostation Building, 12th floor, Suite 1202, BP 64, Sector 5, Saltlake, Kolkata-
                700091</p>
            <p class="mb-0">GSTIN/UIN: 19AACCE8015C1ZY</p>
            <p class="mb-0">State Name: West Bengal, Code: 19</p>
        </div>
        <div class="col-4 text-end">
            <p class="mb-0 fw-bold">Invoice No: <span class="Invoice_Id"></span></p>
            <p class="mb-0 fw-bold">Payment date: <span class="Payment_date"></span></p>
        </div>
    </div>
    <div class="row justify-content-center mt-3 align-items-center">
        <div class="col-12">
            <div style="background: #F5F5F5;border: 1px solid #707070;" class="py-3">
                <p class="mb-0 ms-3"><b>Buyer:</b><span class="buyer_name"></span></p>
                <p class="mb-0 ms-3"><b>Address:</b> <span class="buyer_address"></span></p>
                <p class="mb-0 ms-3"><b>Pin code:</b> <span class="buyer_pincode"></span></p>
                <p class="mb-0 ms-3"><b>COE:</b> <span class="buyer_coe"></span></p>
            </div>
        </div>
    </div>
    <table class="table table-bordered mt-3">
        <tbody id="fix_invoice_html">

        </tbody>
    </table>
    <table class="table table-bordered mt-3">
        <tbody id="fix_invoices_html">
        </tbody>
    </table>
    <p><b> Rounding Off: </b><b class="fix_txt_round"></b></p>
    <p><b> Tax Amount (in words): Indian Rupees </b><b class="fix_txt_inwords"></b></p>
    <div class="row justify-content-center mt-3 align-items-center">
        <div class="col-8">
            <p style="max-width:400px;"><b>Declaration:</b><br /> We declare that this shows the actual price of the
                goods described and that all particulars are true and correct.</p>
        </div>
        <div class="col-4 text-center" style="background-color: #d3d3d38c;border: solid 1px darkgray;height: 165px;">
            <p class="mb-0 fw-bold text-center"
                style="border-bottom: solid 1px darkgray;margin: 0 -13px;font-size: 20px; padding:10px 0;">Payment Mode:
                <span class="lbl_payment_mode"></span></p>
            <p style="font-size: 13px;text-align: center;line-height: 30px;"> <b>For Edudigm Education Services Pvt.
                    Ltd</b></p>
            <img src="eduassets/images/edu_stamp.png" style="width: 165px;" alt="">
        </div>
    </div>
    <footer>
        <div class="row">
            <div class="col-3">
                <p style="font-size: 12px;margin-bottom: 0;">This is a computer generated invoice</p>
            </div>
            <div class="col-9">
                <div class="footerRight">
                    <div class="row">
                        <div class="col-4">
                            <p class="mb-0 text-center text-light" style="font-size: 15px;"><i
                                    class="fas fa-phone-alt"></i> +919230734848</p>
                        </div>
                        <div class="col-4">
                            <p class="mb-0 text-center text-light" style="font-size: 15px;"><i
                                    class="fas fa-envelope"></i> contact@edudigm.in</p>
                        </div>
                        <div class="col-4">
                            <p class="mb-0 text-center text-light" style="font-size: 15px;"><i class="fas fa-globe"></i>
                                edudigm.in</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<p class="printMe">Print</p>
@include('include.footer')
<script>
    $('.printMe').click(function() {
        $('.printMe').hide();
        window.print();
    });
    var fix_invoice_html="";
    var fix_invoices_html="";
    var var_invoice_html="";
    var var_invoices_html="";
    var transaction_id=geturlparam('transaction_id');
    var invoice_response=[];
    var totalVarfees=0;
    var totalFixFees=0;
    $(document).ready(function(){
        invoice_response=invoice_list(transaction_id);
        //invoice_response={"response_code":200,"response_msg":"Invoice Data get Successfully","response_data":{"payment_date":"2022-09-02","buyer_name":"asda","buyer_address":"ff","buyer_coe":null,"buyer_pincode":"401209","product_details":[{"invoice_id":"S\/0219\/23-24","name":"Cool Nerd Posters","item_type":"product","item_quantity":1,"item_price":{"fix_fees":40,"veriable_fee":60}}],"order_price":80,"payment_mode":"online (CC)","hsnsac_code":"987564","cgst_fix":2.5,"sgst_fix":2.5,"cgst_var":9,"sgst_var":9}};
        if(invoice_response.response_code==200){
            var Invoice_Id=invoice_response.response_data.product_details[0].invoice_id;
            var payment_mode=invoice_response.response_data.payment_mode;
            $('.Invoice_Id').text(Invoice_Id);
            $('.Payment_date').text(invoice_response.response_data.payment_date);
            $('.buyer_name').text(invoice_response.response_data.buyer_name);
            $('.buyer_address').text(invoice_response.response_data.buyer_address);
            $('.buyer_pincode').text(invoice_response.response_data.buyer_pincode);
            if(invoice_response.response_data.buyer_coe){
                $('.buyer_coe').text(invoice_response.response_data.buyer_coe);
            }else{
                $('.buyer_coe').parent().hide();
            }
            $('.lbl_payment_mode').text(payment_mode);
            var_populate_invoice();
            $('#fixdiv').hide();
            //fix_populate_invoice();
            // if(totalVarfees && totalVarfees>0){
            //     $('#vardiv').show();
            // }else{
            //     $('#vardiv').hide();
            // }
            // if(totalFixFees && totalFixFees>0){
            //     $('#fixdiv').show();
            // }else{
            //     $('#fixdiv').hide();
            // }
        } 
        //$('.printMe').click();
    });
    function var_populate_invoice(){
        var HSN_SAC="987564";
        var_invoice_html+='<tr>';
        var_invoice_html+='<td class="fw-bold">Description of Service</td>';
        var_invoice_html+='<td class="fw-bold">HSN/SAC</td>';
        var_invoice_html+='<td class="fw-bold">Quantity</td>';
        var_invoice_html+='<td class="fw-bold">Rate</td>';
        var_invoice_html+='<td class="fw-bold">Total</td>';
        var_invoice_html+='</tr>';
        totalVarfees=0;
        $.each(invoice_response.response_data.product_details,function(invoice_key,invoice_value){  
            if(invoice_value.var_hsnsac_code){
                HSN_SAC=invoice_value.var_hsnsac_code;
            }                    
            // totalVarfees=totalVarfees+parseInt(invoice_value.item_price.variable_fee);
            if((invoice_response.response_data.product_details).length>0){
                totalVarfees=totalVarfees+(parseInt(invoice_value.item_price.variable_fee)+parseInt(invoice_value.item_price.fix_fee));
            }else{
                totalVarfees=totalVarfees+parseInt(invoice_response.response_data.order_price);
            }
            var rate=((invoice_value.item_price.variable_fee)/invoice_value.item_quantity)
            var_invoice_html+='<tr>';
            var_invoice_html+='<td>'+invoice_value.name+' </td>';
            var_invoice_html+='<td>'+HSN_SAC+'</td>';
            if(invoice_value.item_type=="course" || invoice_value.item_type=="installment"){
                var_invoice_html+='<td>'+invoice_value.item_quantity+'</td>';
                var_invoice_html+='<td>₹'+totalVarfees+'</td>';
                var_invoice_html+='<td>₹'+totalVarfees+'</td>';
            }else{
                var_invoice_html+='<td>'+invoice_value.item_quantity+'</td>';
                var_invoice_html+='<td>₹'+rate+'</td>';
                var_invoice_html+='<td>₹'+invoice_value.item_price.variable_fee+'</td>';
            }
            
            var_invoice_html+='</tr>';                    
        });
        var order_price=totalVarfees;
        var cgst=invoice_response.response_data.cgst_var;
        var sgst=invoice_response.response_data.sgst_var;

        taxable=Math.round(order_price/1.18);
        cgst_item_amount=Math.round(taxable*cgst/100);
        sgst_item_amount=Math.round(taxable*sgst/100);
        total_tax=cgst_item_amount+sgst_item_amount;
        var roundOff=totalVarfees-(taxable+total_tax);
        $('.var_txt_round').text('₹'+roundOff);
        $('.var_tax_inwords').text(toWords(total_tax) +" Only")
        var_invoice_html+='<tr>';
        var_invoice_html+='<td colspan="4"><b>Total Amount</b></td>';
        var_invoice_html+='<td><b>₹'+totalVarfees+'</b></td>';                    
        var_invoice_html+='</tr>';
        var_invoices_html+='<tr>';
        var_invoices_html+='<td colspan="6" style="font-size: 12px; text-align:left;">Amount Chargeable (in words): <br> <b style="font-size: 20px;">Indian Rupees '+(toWords(order_price) +" Only")+'</b></td>';
        var_invoices_html+='<td colspan="2" style="font-size: 12px; text-align:center;">E.& O.E</td>';
        var_invoices_html+='</tr>';
        var_invoices_html+='<tr>';
        var_invoices_html+='<td rowspan="2">HSN/SAC</td>';
        var_invoices_html+='<td rowspan="2">Taxable Value</td>';
        var_invoices_html+='<td colspan="2">cGST</td>';
        var_invoices_html+='<td colspan="2">sGST</td>';
        var_invoices_html+='<td rowspan="2">Total Tax</td>';
        var_invoices_html+='</tr>';
        var_invoices_html+='<tr>';
        var_invoices_html+='<td>Rate</td>';
        var_invoices_html+='<td>Amount</td>';
        var_invoices_html+='<td>Rate</td>';
        var_invoices_html+='<td>Amount</td>';
        var_invoices_html+='</tr>';
        var_invoices_html+='<tr>';
        var_invoices_html+='<td>'+HSN_SAC+'</td>';
        var_invoices_html+='<td>'+taxable+'</td>';
        var_invoices_html+='<td>'+cgst+'%</td>';
        var_invoices_html+='<td>'+cgst_item_amount+'</td>';
        var_invoices_html+='<td>'+sgst+'%</td>';
        var_invoices_html+='<td>'+sgst_item_amount+'</td>';
        var_invoices_html+='<td>'+total_tax+'</td>';
        var_invoices_html+='</tr>';
        $('#var_invoice_html').html(var_invoice_html);
        $('#var_invoices_html').html(var_invoices_html);
    }
    function fix_populate_invoice(){
        var HSN_SAC="987564";
        fix_invoice_html+='<tr>';
        fix_invoice_html+='<td class="fw-bold">Description of Service</td>';
        fix_invoice_html+='<td class="fw-bold">HSN/SAC</td>';
        fix_invoice_html+='<td class="fw-bold">Quantity</td>';
        fix_invoice_html+='<td class="fw-bold">Rate</td>';
        fix_invoice_html+='<td class="fw-bold">Total</td>';
        fix_invoice_html+='</tr>';
        totalFixFees=0;
        $.each(invoice_response.response_data.product_details,function(invoice_key,invoice_value){
            if(invoice_value.fix_hsnsac_code){
                HSN_SAC=invoice_value.fix_hsnsac_code;
            }                    
            totalFixFees=totalFixFees+parseInt(invoice_value.item_price.fix_fee);
            var rate=((invoice_value.item_price.fix_fee)/invoice_value.item_quantity)
            fix_invoice_html+='<tr>';
            fix_invoice_html+='<td>'+invoice_value.name+' (Study Kit)</td>';
            fix_invoice_html+='<td>'+HSN_SAC+'</td>';
            fix_invoice_html+='<td>'+invoice_value.item_quantity+'</td>';
            fix_invoice_html+='<td>₹'+rate+'</td>';
            fix_invoice_html+='<td>₹'+invoice_value.item_price.fix_fee+'</td>';
            fix_invoice_html+='</tr>';                    
        });
        var order_price=totalFixFees;
        var cgst=invoice_response.response_data.cgst_fix;
        var sgst=invoice_response.response_data.sgst_fix;
        taxable=Math.round(order_price/1.05);
        cgst_item_amount=Math.round(taxable*cgst/100);
        sgst_item_amount=Math.round(taxable*sgst/100);
        total_tax=cgst_item_amount+sgst_item_amount;
        var roundOff=totalFixFees-(taxable+total_tax);
        $('.fix_txt_round').text('₹'+roundOff);
        $('.fix_txt_inwords').text(toWords(total_tax) +" Only")
        fix_invoice_html+='<tr>';
        fix_invoice_html+='<td colspan="4"><b>Total Amount</b></td>';
        fix_invoice_html+='<td><b>₹'+totalFixFees+'</b></td>';                    
        fix_invoice_html+='</tr>';
        fix_invoices_html+='<tr>';
        fix_invoices_html+='<td colspan="6" style="font-size: 12px; text-align:left;">Amount Chargeable (in words): <br> <b style="font-size: 20px;">Indian Rupees '+(toWords(order_price) +" Only")+'</b></td>';
        fix_invoices_html+='<td colspan="2" style="font-size: 12px; text-align:center;">E.& O.E</td>';
        fix_invoices_html+='</tr>';
        fix_invoices_html+='<tr>';
        fix_invoices_html+='<td rowspan="2">HSN/SAC</td>';
        fix_invoices_html+='<td rowspan="2">Taxable Value</td>';
        fix_invoices_html+='<td colspan="2">cGST</td>';
        fix_invoices_html+='<td colspan="2">sGST</td>';
        fix_invoices_html+='<td rowspan="2">Total Tax</td>';
        fix_invoices_html+='</tr>';
        fix_invoices_html+='<tr>';
        fix_invoices_html+='<td>Rate</td>';
        fix_invoices_html+='<td>Amount</td>';
        fix_invoices_html+='<td>Rate</td>';
        fix_invoices_html+='<td>Amount</td>';
        fix_invoices_html+='</tr>';
        fix_invoices_html+='<tr>';
        fix_invoices_html+='<td>'+HSN_SAC+'</td>';
        fix_invoices_html+='<td>'+taxable+'</td>';
        fix_invoices_html+='<td>'+cgst+'%</td>';
        fix_invoices_html+='<td>'+cgst_item_amount+'</td>';
        fix_invoices_html+='<td>'+sgst+'%</td>';
        fix_invoices_html+='<td>'+sgst_item_amount+'</td>';
        fix_invoices_html+='<td>'+total_tax+'</td>';
        fix_invoices_html+='</tr>';
        $('#fix_invoice_html').html(fix_invoice_html);
        $('#fix_invoices_html').html(fix_invoices_html);
    }
    function invoice_list() {
        return $.parseJSON($.ajax({
                type: "GET",
                url: 'https://crm.edudigm.com/api/get-new-invoice/'+transaction_id,
                data: {},
                dataType: "json",
                async: !1,
                headers: {
                    "Content-Type": "application/json"
                }
            })
            .responseText)
    }
    function geturlparam(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
        return false;
    }
    var th = ['','Thousand','Million', 'Billion','Trillion'];
    var dg = ['Zero','One','Two','Three','Four', 'Five','Six','Seven','Eight','Nine']; 
    var tn = ['Ten','Eleven','Twelve','Thirteen', 'Fourteen','Fifteen','Sixteen', 'Seventeen','Eighteen','Nineteen']; 
    var tw = ['Twenty','Thirty','Forty','Fifty', 'Sixty','Seventy','Eighty','Ninety']; 
    function toWords(s){s = s.toString(); s = s.replace(/[\, ]/g,''); if (s != parseFloat(s)) return 'not a number'; var x = s.indexOf('.'); if (x == -1) x = s.length; if (x > 15) return 'too big'; var n = s.split(''); var str = ''; var sk = 0; for (var i=0; i < x; i++) {if ((x-i)%3==2) {if (n[i] == '1') {str += tn[Number(n[i+1])] + ' '; i++; sk=1;} else if (n[i]!=0) {str += tw[n[i]-2] + ' ';sk=1;}} else if (n[i]!=0) {str += dg[n[i]] +' '; if ((x-i)%3==0) str += 'Hundred ';sk=1;} if ((x-i)%3==1) {if (sk) str += th[(x-i-1)/3] + ' ';sk=0;}} if (x != s.length) {var y = s.length; str += 'point '; for (var i=x+1; i<y; i++) str += dg[n[i]] +' ';} return str.replace(/\s+/g,' ');}   

</script>