<title>Home</title>
@include('include.header')
<style>
    .spanone,
    .spantwo,
    .sliderMenu .images img {
        display: none
    }

    .sentence {
        position: absolute;
        bottom: 0;
        width: 100%;
        text-align: center;
        color: #fff;
    }

    :root {
        --col1: #FF6100;
    }

    * {
        font-family: "Poppins", sans-serif;
    }

    .bg1 {
        background-color: var(--col1);
    }

    .col1 {
        color: var(--col1);
    }

    .topmenuLogo {
        width: 150px;
    }

    .navbar {
        transition: 0.4s;
    }

    .nav-link.active {
        font-weight: 700;
    }

    .headerText {
        font-size: 3vw;
        text-align: center;
        height: 60px;
        overflow: hidden;
        margin-top: -150px;
        color: #fff;
    }

    @media only screen and (max-device-width:500px) {
        .headerText {
            margin-top: -8% !important;
            height: auto;
        }

        .clientPartnerImages {
            width: 100% !important;
            height: 100% !important;
        }
    }

    .headerText .withText {
        margin-left: 15vw;
    }

    .leftRightText .para {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        font-size: 3vw;
        text-align: center;
        font-weight: 600;
    }

    .leftRightText .para span {
        font-size: 5vw;
    }

    .btn_primary {
        display: inline-block;
        background-color: var(--col1);
        font-size: 3vw;
        padding: 1vw 3vw;
        border-radius: 15px;
        color: #fff;
        cursor: pointer;
        text-decoration: none;
        border: 1px solid var(--col1);
        transition: all ease .3s;
    }

    .btn_primary:hover {
        color: var(--col1);
        background-color: #fff;
    }

    .toughBoring .para {
        font-size: 2.5vw;
    }

    .toughBoaringCard {
        background-color: #F2F0F4;
        padding: 2vw 2vw;
        border-radius: 15px;
    }

    .toughBoaringCard img {
        width: 50%;
    }

    .journey .para {
        font-weight: 600;
    }

    .eduStarBg {
        background: linear-gradient(144deg, rgba(255, 97, 1, 1) 0%, rgba(225, 165, 57, 1) 50%);
        position: relative;
        padding: 15px 0 45px 0;
        border-radius: 15px;
    }

    .eduStarBg .btn_viewMore {
        position: absolute;
        bottom: 0;
        width: 100%;
        text-align: center;
        background-color: #00000078;
        border-radius: 0px 0px 15px 15px;
        color: #fff;
        padding: 5px 0;
        cursor: pointer;
    }

    .statsCard {
        background-color: #fff;
        border-radius: 10px;
    }

    .statsBg {
        background-size: 250%;
        background-repeat: no-repeat;
        background-position: center;
    }

    .imgStatsPerson {
        position: absolute;
        width: 150px;
        bottom: 0;
    }

    .stats_person1 {
        left: 5%;
    }

    .stats_person2 {
        right: 5%;
    }

    .clientPartnerImages {
        width: 100px;
        height: 100px;
        margin-bottom: 25px;
        filter: grayscale(100%);
        transition: all ease .5s;
    }

    .clientPartnerImages:hover {
        filter: grayscale(0%);
    }

    .awardsImages {
        max-width: 50%;
        position: relative;
    }

    .imageCarousel img{
        display: none;
    }
    .imageCarousel img.active{
        display: block;
    }
</style>
<style>
    .cd-words-wrapper {
        display: inline-block;
        position: relative;
    }

    @media only screen and (max-width: 450px) {
        .cd-words-wrapper {}

        .cd-words-wrapper b {
            padding: 0;
        }
    }

    .cd-words-wrapper b {
        display: inline-block;
        position: absolute;
        white-space: nowrap;
        padding: 0 0.2em;
    }

    @media (max-width: 480px) {
        .cd-words-wrapper b {
            white-space: initial;
        }
    }

    .cd-words-wrapper b.is-visible {
        position: relative;
    }

    .no-js .cd-words-wrapper b {
        opacity: 0;
    }

    .no-js .cd-words-wrapper b.is-visible {
        opacity: 1;
    }

    .cd-headline.slide span {
        display: inline-block;
        padding: .2em 0.5em;
    }

    .cd-headline.slide .cd-words-wrapper {
        overflow: hidden;
        vertical-align: top;
    }

    .cd-headline.slide span {
        display: inline-block;
        padding: .2em 0.5em;
    }

    .cd-headline.slide .cd-words-wrapper {
        overflow: hidden;
        vertical-align: top;
    }

    .cd-headline.slide b {
        opacity: 0;
        top: .2em;
    }

    .cd-headline.slide b.is-visible {
        top: 0;
        opacity: 1;
        -webkit-animation: slide-in 0.6s;
        -moz-animation: slide-in 0.6s;
        animation: slide-in 0.6s;
    }

    .cd-headline.slide b.is-hidden {
        -webkit-animation: slide-out 0.6s;
        -moz-animation: slide-out 0.6s;
        animation: slide-out 0.6s;
    }

    @-webkit-keyframes slide-in {
        0% {
            opacity: 0;
            -webkit-transform: translateY(-100%);
        }

        60% {
            opacity: 1;
            -webkit-transform: translateY(20%);
        }

        100% {
            opacity: 1;
            -webkit-transform: translateY(0);
        }
    }

    @-moz-keyframes slide-in {
        0% {
            opacity: 0;
            -moz-transform: translateY(-100%);
        }

        60% {
            opacity: 1;
            -moz-transform: translateY(20%);
        }

        100% {
            opacity: 1;
            -moz-transform: translateY(0);
        }
    }

    @keyframes slide-in {
        0% {
            opacity: 0;
            -webkit-transform: translateY(-100%);
            -moz-transform: translateY(-100%);
            -ms-transform: translateY(-100%);
            -o-transform: translateY(-100%);
            transform: translateY(-100%);
        }

        60% {
            opacity: 1;
            -webkit-transform: translateY(20%);
            -moz-transform: translateY(20%);
            -ms-transform: translateY(20%);
            -o-transform: translateY(20%);
            transform: translateY(20%);
        }

        100% {
            opacity: 1;
            -webkit-transform: translateY(0);
            -moz-transform: translateY(0);
            -ms-transform: translateY(0);
            -o-transform: translateY(0);
            transform: translateY(0);
        }
    }

    @-webkit-keyframes slide-out {
        0% {
            opacity: 1;
            -webkit-transform: translateY(0);
        }

        60% {
            opacity: 0;
            -webkit-transform: translateY(120%);
        }

        100% {
            opacity: 0;
            -webkit-transform: translateY(100%);
        }
    }

    @-moz-keyframes slide-out {
        0% {
            opacity: 1;
            -moz-transform: translateY(0);
        }

        60% {
            opacity: 0;
            -moz-transform: translateY(120%);
        }

        100% {
            opacity: 0;
            -moz-transform: translateY(100%);
        }
    }

    @keyframes slide-out {
        0% {
            opacity: 1;
            -webkit-transform: translateY(0);
            -moz-transform: translateY(0);
            -ms-transform: translateY(0);
            -o-transform: translateY(0);
            transform: translateY(0);
        }

        60% {
            opacity: 0;
            -webkit-transform: translateY(120%);
            -moz-transform: translateY(120%);
            -ms-transform: translateY(120%);
            -o-transform: translateY(120%);
            transform: translateY(120%);
        }

        100% {
            opacity: 0;
            -webkit-transform: translateY(100%);
            -moz-transform: translateY(100%);
            -ms-transform: translateY(100%);
            -o-transform: translateY(100%);
            transform: translateY(100%);
        }
    }
</style>
<div class="content">
    <div class="top_menu_header">
        @include('include.top_menu')
    </div>
</div>
{{-- <div class="sliderMenu position-relative mt-4 pt-5 d-none">
    <div class="images"></div>
    <h2 class="sentence">15 YEARS OF <span class="slidingVertical text1"></span> WITH <span
            class="slidingVertical text2"></span></h2>
</div> --}}
<div class="sliderMenu position-relative mt-4 pt-5">
    <div class="imageCarousel">
        <img src="{{ asset('assets/images/banner1.png') }}" class="w-100 active" alt="...">
        <img src="{{ asset('assets/images/banner2.png') }}" class="w-100" alt="...">
        <img src="{{ asset('assets/images/banner3.png') }}" class="w-100" alt="...">
        <img src="{{ asset('assets/images/banner4.png') }}" class="w-100" alt="...">
    </div>
    <div style="position: absolute;bottom: 0;color: #fff;left: 50%;transform: translate(-50%, 0);">
        <h1 class="cd-headline slide">
            <span class="cd-words-wrapper">
                <b class="is-visible">15 YEARS OF ROLLING WITH PHYSICS</b>
                <b class="is-hidden">15 YEARS OF BONDING WITH CHEMISTRY</b>
                <b class="is-hidden">15 YEARS OF EVOLVING WITH BIOLOGY</b>
                <b class="is-hidden">15 YEARS OF DANCING WITH MATHEMATICS</b>
            </span>
        </h1>
    </div>
</div>
<section>
    <iframe src="https://www.youtube.com/embed/khlbomFl6Gc" title="YouTube video player" frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
        allowfullscreen="" style="height: 300px; width: 100%;"></iframe>
</section>
<div class="position-relative leftRightText"><img src="{{ asset('assets/images/student_left_right.png') }}" alt=""
        class="img-fluid">
    <p class="para"><span>50,000+</span> students from classes 3 to 12 have benefitted from our Inspiring &amp;
        Enjoyable Science &amp; Mathematics Experiential Learning Programs.</p>
</div>
<div class="text-center my-md-4"><span class="btn_primary">Start your Journey</span></div>
<div class="container my-4 toughBoring">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-8">
            <p class="col1 text-center para">Most students find science and mathematics tough and/or boring. Two
                major reasons responsible for this are</p>
        </div>
    </div>
</div>
<div class="container my-4">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-5">
                    <div class="toughBoaringCard mb-4 text-center"><img
                            src="{{ asset('assets/images/toughBoring1.png') }}" class="img-fluid" alt="">
                        <p class="mt-4">An overwhelming focus and pressure to get good marks/ranks which takes the
                            joy out of learning and creates an atmosphere of fear and stress</p>
                    </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-5">
                    <div class="toughBoaringCard mb-4 text-center"><img
                            src="{{ asset('assets/images/toughBoring2.png') }}" class="img-fluid" alt="">
                        <p class="mt-4">The current curriculum and teaching methods being highly theoretical,
                            outdated and disconnected from the realities of a rapidly changing world of the 21st
                            century</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-8">
            <p class="text-center fw-bold">At a time when the world needs problem solvers who can think critically
                and in a scientific manner, we find that the entire education system is falling short of motivating
                and preparing our students to become original thinkers and innovators of tomorrow.</p>
        </div>
    </div>
</div>
<div><img src="{{ asset('assets/images/edudigm_banner.png') }}" alt="" class="img-fluid"></div>
<div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-10">
            <p class="text-center fw-bold">Since 2009, Edudigm, an IIT Kharagpur alumni initiative, has been working
                towards making science &amp; mathematics interesting, fun and enjoyable for thousands of school
                students across the world. We inspire students to get started on a journey where they are not only
                fascinated by the beautiful world of science &amp; mathematics, but are also motivated to explore
                the concepts and their connections with the world. As outcomes, our students achieve their true
                potential and showcase it via their performance and creations across all levels
                (national/international) and formats (subjective/objective) of examinations and competitions across
                the world.</p>
        </div>
    </div>
</div>
<div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-6"><img src="{{ asset('assets/images/pyramid.png') }}" alt="" class="img-fluid"></div>
    </div>
</div>
<div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-10">
            <p class="text-center fw-bold">The secret sauce behind Edudigm’s success over the last 15 years is 4E’s
                framework. The 4E’s framework is a unique methodology of education that focuses on reversing the
                existing paradigm of focusing only on the outcomes of ranks/marks/grades. Our philosophy is inspired
                by Bhagwad Gita’s, Karmanye Vade Karasthye, Ma faleshu Kadanchana (Work without expectation of
                outcomes). We are deeply focused on the process of learning with the primary objective of igniting
                the interest and internal potential hidden in every student and putting them on a track of
                self-improvement. This process of the pursuit of excellence leads to an individual who is a dreamer,
                thinker and a doer. He/she is someone who is ready to not only face the challenges of the 21st
                century but also to solve them for the larger benefit of the society. Success across different
                external yard-sticks, be it exams, careers or jobs is therefore a by-product of the 4E’s model.</p>
        </div>
    </div>
</div>
<div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-10">
            <h1 class="text-center fw-bold">Choose Your <span class="col1">Program</span></h1>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-12">
            @include('include.ClassProgram', ['type' => 0])
        </div>
    </div>
</div>
{{-- <div class="container my-4 journey">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-3"><img src="{{ asset('assets/images/edustore_logo.png') }}" alt="" class="img-fluid"></div>
    </div>
</div> --}}
@include('include.commonSection')
<div class="bottom_footer_menu">
    @include('include.bottom_footer')
</div>
@include('include.footer')
<script>
    var myCarousel = document.querySelector('#carouselExampleFade')
    var carousel = new bootstrap.Carousel(myCarousel, {
        interval: 500,
        wrap: true
    })
    // var imageIndex=0;
    var bannerJson=[{"text1":"ROLLING","text2":"PHYSICS","image":"{{ asset('assets/images/banner1.png') }}"},
    {"text1":"BONDING","text2":"CHEMISTRY","image":"{{ asset('assets/images/banner2.png') }}"},
    {"text1":"EVOLVING","text2":"BIOLOGY","image":"{{ asset('assets/images/banner3.png') }}"},
    {"text1":"DANCING","text2":"MATHEMATICS","image":"{{ asset('assets/images/banner4.png') }}"}];
    // var sliderImageText1="";
    // var sliderImageText2="";
    // var allImages="";
    // var allLength=(bannerJson.length);    
    // bannerJson.forEach((value,index) => {          
    //     sliderImageText1+='<span class="spanone">'+value.text1+'</span>';
    //     sliderImageText2+='<span class="spantwo">'+value.text2+'</span>';
    //     allImages+='<img src="'+value.image+'" class="img-fluid" alt="">';        
    // });
    // $('.slidingVertical.text1').html(sliderImageText1);
    // $('.slidingVertical.text2').html(sliderImageText2);
    // $('.sliderMenu .images').html(allImages);
    // setInterval(() => {
    //     $('.slidingVertical.text1 span,.slidingVertical.text2 span,.sliderMenu .images img').hide();
    //     $('.slidingVertical.text1 span').eq(imageIndex).fadeIn(300);
    //     $('.slidingVertical.text2 span').eq(imageIndex).fadeIn(300);
    //     $('.sliderMenu .images img').eq(imageIndex).fadeIn(300);
    //     imageIndex++        
    //     if(imageIndex==allLength){
    //         imageIndex=0;
    //     }
    // }, 3000);
</script>
<script>
    var animationDelay = 3000,
        //loading bar effect
        barAnimationDelay = 3500,
        barWaiting = barAnimationDelay -
        3000, //3000 is the duration of the transition on the loading bar - set in the scss/css file
        //letters effect
        lettersDelay = 50,
        //type effect
        typeLettersDelay = 150,
        selectionDuration = 500,
        typeAnimationDelay = selectionDuration + 800,
        //clip effect 
        revealDuration = 600,
        revealAnimationDelay = 1500;

    initHeadline();
    setInterval(() => {
        var activeIndex=$('.imageCarousel img.active').index()+1;
        var totalimgae=$('.imageCarousel img').length;
        if(activeIndex==totalimgae){
            $('.imageCarousel img').removeClass('active');
            $('.imageCarousel img').eq(0).addClass('active');
        }else{
            $('.imageCarousel img').removeClass('active');
            $('.imageCarousel img').eq(activeIndex).addClass('active');
        }
    }, animationDelay);

    function initHeadline() {
        //insert <i> element for each letter of a changing word
        singleLetters($('.cd-headline.letters').find('b'));
        //initialise headline animation
        animateHeadline($('.cd-headline'));
    }
    function singleLetters($words) {
        $words.each(function () {
            var word = $(this),
                letters = word.text().split(''),
                selected = word.hasClass('is-visible');
            for (i in letters) {
                if (word.parents('.rotate-2').length > 0) letters[i] = '<em>' + letters[i] + '</em>';
                letters[i] = (selected) ? '<i class="in">' + letters[i] + '</i>' : '<i>' + letters[i] +
                    '</i>';
            }
            var newLetters = letters.join('');
            word.html(newLetters).css('opacity', 1);
        });
    }
    function animateHeadline($headlines) {
        var duration = animationDelay;
        $headlines.each(function () {
            var headline = $(this);

            if (headline.hasClass('loading-bar')) {
                duration = barAnimationDelay;
                setTimeout(function () {
                    headline.find('.cd-words-wrapper').addClass('is-loading')
                }, barWaiting);
            } else if (headline.hasClass('clip')) {
                var spanWrapper = headline.find('.cd-words-wrapper'),
                    newWidth = spanWrapper.width() + 10
                spanWrapper.css('width', newWidth);
            } else if (!headline.hasClass('type')) {
                //assign to .cd-words-wrapper the width of its longest word
                var words = headline.find('.cd-words-wrapper b'),
                    width = 0;
                words.each(function () {
                    var wordWidth = $(this).width();
                    if (wordWidth > width) width = wordWidth;
                });
                headline.find('.cd-words-wrapper').css('width', width);
            };

            //trigger animation
            setTimeout(function () {
                hideWord(headline.find('.is-visible').eq(0))
            }, duration);
        });
    }
    function hideWord($word) {
        var nextWord = takeNext($word);
        if ($word.parents('.cd-headline').hasClass('type')) {
            var parentSpan = $word.parent('.cd-words-wrapper');
            parentSpan.addClass('selected').removeClass('waiting');
            setTimeout(function () {
                parentSpan.removeClass('selected');
                $word.removeClass('is-visible').addClass('is-hidden').children('i').removeClass('in')
                    .addClass('out');
            }, selectionDuration);
            setTimeout(function () {
                showWord(nextWord, typeLettersDelay)
            }, typeAnimationDelay);

        } else if ($word.parents('.cd-headline').hasClass('letters')) {
            var bool = ($word.children('i').length >= nextWord.children('i').length) ? true : false;
            hideLetter($word.find('i').eq(0), $word, bool, lettersDelay);
            showLetter(nextWord.find('i').eq(0), nextWord, bool, lettersDelay);

        } else if ($word.parents('.cd-headline').hasClass('clip')) {
            $word.parents('.cd-words-wrapper').animate({
                width: '2px'
            }, revealDuration, function () {
                switchWord($word, nextWord);
                showWord(nextWord);
            });

        } else if ($word.parents('.cd-headline').hasClass('loading-bar')) {
            $word.parents('.cd-words-wrapper').removeClass('is-loading');
            switchWord($word, nextWord);
            setTimeout(function () {
                hideWord(nextWord)
            }, barAnimationDelay);
            setTimeout(function () {
                $word.parents('.cd-words-wrapper').addClass('is-loading')
            }, barWaiting);

        } else {
            switchWord($word, nextWord);
            setTimeout(function () {
                hideWord(nextWord)
            }, animationDelay);
        }
    }
    function showWord($word, $duration) {
        if ($word.parents('.cd-headline').hasClass('type')) {
            showLetter($word.find('i').eq(0), $word, false, $duration);
            $word.addClass('is-visible').removeClass('is-hidden');

        } else if ($word.parents('.cd-headline').hasClass('clip')) {
            $word.parents('.cd-words-wrapper').animate({
                'width': $word.width() + 10
            }, revealDuration, function () {
                setTimeout(function () {
                    hideWord($word)
                }, revealAnimationDelay);
            });
        }
    }
    function hideLetter($letter, $word, $bool, $duration) {
        $letter.removeClass('in').addClass('out');
        if (!$letter.is(':last-child')) {
            setTimeout(function () {
                hideLetter($letter.next(), $word, $bool, $duration);
            }, $duration);
        } else if ($bool) {
            setTimeout(function () {
                hideWord(takeNext($word))
            }, animationDelay);
        }
        if ($letter.is(':last-child') && $('html').hasClass('no-csstransitions')) {
            var nextWord = takeNext($word);
            switchWord($word, nextWord);
        }
    }
    function showLetter($letter, $word, $bool, $duration) {
        $letter.addClass('in').removeClass('out');
        if (!$letter.is(':last-child')) {
            setTimeout(function () {
                showLetter($letter.next(), $word, $bool, $duration);
            }, $duration);
        } else {
            if ($word.parents('.cd-headline').hasClass('type')) {
                setTimeout(function () {
                    $word.parents('.cd-words-wrapper').addClass('waiting');
                }, 200);
            }
            if (!$bool) {
                setTimeout(function () {
                    hideWord($word)
                }, animationDelay)
            }
        }
    }
    function takeNext($word) {
        return (!$word.is(':last-child')) ? $word.next() : $word.parent().children().eq(0);
    }
    function takePrev($word) {
        return (!$word.is(':first-child')) ? $word.prev() : $word.parent().children().last();
    }
    function switchWord($oldWord, $newWord) {
        $oldWord.removeClass('is-visible').addClass('is-hidden');
        $newWord.removeClass('is-hidden').addClass('is-visible');
    }
</script>