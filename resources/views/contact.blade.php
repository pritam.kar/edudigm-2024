<title>Home</title>
@include('include.header')
<div class="content">
    <div class="top_menu_header">
        @include('include.top_menu')
    </div>
</div>
<div class="container mt-5 pt-5">
    <div class="row">
        <div class="col-12">
            @include('include.Contact')
        </div>
    </div>
</div>
<div class="bottom_footer_menu">
    @include('include.bottom_footer')
</div>
@include('include.footer')