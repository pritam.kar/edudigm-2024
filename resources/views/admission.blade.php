<title>Home</title>
@include('include.header')
<style>
    .rightSideCard {
        background-color: var(--col2);
        border: 4px solid var(--col1);
        border-radius: 5px;
        position: fixed;
        top: 100px;
        width: 23%;
        z-index: 1;
    }

    .card.duration {
        cursor: pointer;
    }

    .card.duration.active {
        background-color: var(--col1);
        border: 1px solid #9A9A9A;
        color: #ffffff;
    }

    .small,
    small {
        font-size: 10px;
    }
</style>
<div class="content">
    <div class="top_menu_header">
        @include('include.top_menu')
    </div>
</div>
<div class="container-fluid pt-5 mt-5">
    <div class="row">
        <div class="col-md-9">
            <div class="card px-4 py-4">
                <div class="row">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="txt_stu_name" class="form-label mb-0">Name <span
                                    class="text-danger">*</span></label>
                            <input type="text" id="txt_stu_name" class=" form-control">
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="mb-3">
                            <label for="txt_stu_mobile" class="form-label mb-0">Mobile <span
                                    class="text-danger">*</span></label>
                            <input type="text" class="mobile form-control" minlength="10" maxlength="10"
                                onkeypress="return (event.charCode !=8 &amp;&amp; event.charCode ==0 || (event.charCode >= 48 &amp;&amp; event.charCode <= 57))"
                                id="txt_stu_mobile" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="txt_stu_email" class="form-label mb-0">Email <span
                                    class="text-danger">*</span></label>
                            <input type="text" id="txt_stu_email" class=" form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="class_selector" class="form-label mb-0">Class <span
                                    class="text-danger">*</span></label>
                            <select id="class_selector" class="form-select">
                                <option value="" disabled="" selected="">Select Class</option>
                                <option value="III">III</option>
                                <option value="IV">IV</option>
                                <option value="V">V</option>
                                <option value="VI">VI</option>
                                <option value="VII">VII</option>
                                <option value="VIII">VIII</option>
                                <option value="IX">IX</option>
                                <option value="X">X</option>
                                <option value="XI">XI</option>
                                <option value="XII">XII</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 stu_program">
                        <div class="mb-3">
                            <label for="program_selector" class="form-label mb-0">Select Program <span
                                    class="text-danger">*</span></label>
                            <select id="program_selector" class="form-select">
                                <option value="" disabled="" selected="">Select Program</option>
                                <option value="gl">Guided Learning Programs</option>
                                <option value="spl">Self-Paced Learning Programs
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 st_sub_combi">
                        <div class="mb-3 ">
                            <label for="subj_selector" class="form-label mb-0">Choose Subject Combination <span
                                    class="text-danger">*</span></label>
                            <select id="subj_selector" class="form-select" disabled>
                                <option value="PCMB">PCMB</option>
                                <option value="PCM">PCM</option>
                                <option value="PCB">PCB</option>
                                <option value="MS">MS</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 stu_course">
                        <div class="mb-3">
                            <label for="course_selector" class="form-label mb-0">Select Course <span
                                    class="text-danger">*</span></label>
                            <select id="course_selector" class="form-select">

                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 stu_course">
                        <div class="mb-3">
                            <label for="duration_selector" class="form-label mb-0">Select Duration <span
                                    class="text-danger">*</span></label>
                            <select id="duration_selector" class="form-select">

                            </select>
                        </div>
                    </div>
                </div>
                <div class="row notSelfPaced" id="installment_Structure">
                    <div class="col-md-12">
                        <h4 class="text-center fw-bold">Installment Structure</h4>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-center text-md-start fw-bold">Quaterly Installment Structure</h6>
                        <div id="quaterlyInstDiv"></div>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-center text-md-start fw-bold">Monthly Installment Structure</h6>
                        <div id="monthlyInstDiv"></div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="rightSideCard px-2 py-4">
                <p class="col1 fw-bold h5 text-center">Payment Summary</p>
                <div class="row notSelfPaced">
                    <div class="col-md-8">
                        <h6 class="text-center text-md-start">Registration Fees</h6>
                    </div>
                    <div class="col-md-4">
                        <h6 class="text-center text-md-end"><span id="reg_fee"></span></h6>
                    </div>
                </div>
                <div class="row notSelfPaced">
                    <div class="col-md-8">
                        <h6 class="text-center text-md-start mb-0">Seat Booking Fees</h6>
                        <small id="ref_seat_book"></small>
                    </div>
                    <div class="col-md-4">
                        <h6 class="text-center text-md-end"><span id="set_fee"></span></h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <h6 class="text-center text-md-start mb-0">Variable Fees</h6>
                        <small id="ref_var_fees"></small>
                    </div>
                    <div class="col-md-4">
                        <h6 class="text-center text-md-end"><span id="var_fee"></span></h6>
                    </div>
                </div>
                <hr class="m-0 mb-2">
                <div class="row">
                    <div class="col-md-8">
                        <h6 class="text-center text-md-start fw-bold">Sub Total</h6>
                    </div>
                    <div class="col-md-4">
                        <h6 class="text-center text-md-end"><span id="total_fees"></span></h6>
                    </div>
                </div>
                <div class="row justify-content-center align-items-center">
                    <div class="col-md-6">
                        <h6 class="text-center text-md-start fw-bold">One Time</h6>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-center text-center"><span id="lbl_onetimeAmt" class="btn1"
                                onclick="paymentClick('onetime')"></span></h6>
                    </div>
                </div>
                <div class="row notSelfPaced justify-content-center align-items-center">
                    <div class="col-md-6">
                        <h6 class="text-center text-md-start fw-bold">Quaterly</h6>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-center text-center"><span id="lbl_quaterlyAmt" class="btn1"
                                onclick="paymentClick('quarterly')"></span></h6>
                    </div>
                </div>
                <div class="row notSelfPaced justify-content-center align-items-center">
                    <div class="col-md-6">
                        <h6 class="text-center text-md-start fw-bold">Monthly</h6>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-center text-center"><span id="lbl_monthlyAmt" class="btn1"
                                onclick="paymentClick('monthly')"></span></h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container mt-5">
    <div class="row">
        <div class="col-12">
            @include('include.Contact')
        </div>
    </div>
</div>
<div class="bottom_footer_menu">
    @include('include.bottom_footer')
</div>
<div id="payUform" class="d-none"></div>
<div class="modal fade" id="mdl_loader" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" style="background-color: transparent;border: 0;">
            <img src="{{ asset('assets/images/spinnnnnn.gif') }}" style="width: 200px;margin:0 auto;" alt="">
        </div>
    </div>
</div>
@include('include.footer')
<script>
    var studentClass=getUrlParam('class');
    var admissionType=getUrlParam('type');
    var regAmt=0;
    var seatAmt=0;
    var varAmt=0;
    var payableOneTime=0;
    var payableQuaterly=0;
    var payableMonthly=0;
    var feesId="";
    $(document).ready(function () {
        if(studentClass){
            $('#class_selector').val(studentClass);
        }
        if(admissionType){
            $('#program_selector').val(admissionType);
        }
        classProgram()
        getcourses();        
    });    
    $('#class_selector,#program_selector').on('change', function() {
        classProgram();
    });    
    $('#duration_selector').on('change',
        function() {
            var duration = $("#duration_selector option:selected");
            console.log(duration.data("fees"));
            $("#total_fees").text(duration.data("fees").total_fees);
            $("#var_fee").text(duration.data("fees").variable_fees);
            $("#var_fee").attr("data-amount", duration.data("fees").variable_fees);
            $("#set_fee").text(duration.data("fees").seat_booking_fee);
            $("#reg_fee").text(duration.data("fees").reg_fee);
            regAmt=duration.data("fees").reg_fee;
            seatAmt=duration.data("fees").seat_booking_fee;
            varAmt=duration.data("fees").variable_fees;
            generateInstallment();
        }
    );
    $('#course_selector').on('change',
        function() {
            var combinations = $("#course_selector option:selected").data("combination");
            $.each(combinations, function(i, opt) {
                $("#duration_selector").append("<option value='" + opt.id +
                    "' data-fees='" + opt.total_fees + "'>" + opt.duration +
                    "</option>");
            });
            $("#total_fees").text(combinations[0].total_fees);
            $("#var_fee").text(combinations[0].variable_fees);
            $("#var_fee").attr("data-amount", combinations[0].variable_fees);
            $("#set_fee").text(combinations[0].seat_booking_fee);
            $("#reg_fee").text(combinations[0].reg_fee);
            regAmt=combinations[0].reg_fee;
            seatAmt=combinations[0].seat_booking_fee;
            varAmt=combinations[0].variable_fees;
            generateInstallment();
        }
    );    
    $('#subj_selector').on('change', function() {
        var subject = $('#subj_selector').val();
        getcourses();
    });
    function getcourses() {
        var class_name = $('#class_selector').val();
        var program = $('#program_selector').val();
        var subject = $('#subj_selector').val();
        if (class_name && program && subject) {
            var stu_data = {
                _token: "{{ csrf_token() }}",
                class_name: class_name,
                program: program,
                subject: subject,
            }
            $.ajax({
                type: "post",
                url: "{{ route('getCourses') }}",
                data: stu_data,
                success: function(response) {
                    var data = response.response_data;
                    if (data != null) {
                        $('#duration_selector').prop('disabled', false);
                        $('#course_selector').prop('disabled', false);
                        $("#course_selector").empty();
                        $.each(data, function(index, option) {
                            $("#course_selector").append("<option value='" + option.id +
                                "' data-combination='" + JSON.stringify(option
                                    .course_combinations) + "'>" +
                                option
                                .name + "</option>");
                        });
                        var course_val = $('#course_selector').val();
                        var cominations = data.filter(x => x.id == course_val);
                        if (cominations[0]) {
                            $("#total_fees").text('');
                            $("#duration_selector").empty();
                            $.each(cominations[0].course_combinations, function(i, opt) {
                                $("#duration_selector").append("<option value='" + opt.id +
                                    "' data-fees='" + JSON.stringify(opt) + "'>" + opt.duration +
                                    "</option>");
                            });
                            $("#total_fees").text(cominations[0].course_combinations[0].total_fees);
                            $("#var_fee").text(cominations[0].course_combinations[0].variable_fees);
                            $("#set_fee").text(cominations[0].course_combinations[0].seat_booking_fee);
                            $("#reg_fee").text(cominations[0].course_combinations[0].reg_fee);    
                            regAmt=cominations[0].course_combinations[0].reg_fee;
                            seatAmt=cominations[0].course_combinations[0].seat_booking_fee;
                            varAmt=cominations[0].course_combinations[0].variable_fees;
                            generateInstallment();
                        }
                    }
                }
            })
        }
    }
    function classProgram(){
        var class_name = $('#class_selector').val();
        $("#subj_selector option[value='MS']").attr("disabled", false);
        if (class_name == "XII" || class_name == "XI") {
            $("#subj_selector option[value='MS']").attr("disabled", true);
            $('#subj_selector').val('PCMB');
            $('#subj_selector').prop('disabled', false);
            $('#lbl_req_subject').show();
        } else if (class_name == "III" || class_name == "IV" || class_name == "V") {
            $('#subj_selector').val('MS');
            $('#subj_selector').prop('disabled', true);
            $('#lbl_req_subject').hide();
        } else if (class_name == "VI" || class_name == "VII" || class_name == "VIII" || class_name == "IX" || class_name == "X") {
            $('#subj_selector').val('PCMB');
            $('#subj_selector').prop('disabled', true);
            $('#lbl_req_subject').hide();
        }
        $('#subj_selector').trigger("change");
        getcourses();
        generateInstallment();
        var selected_program=$('#program_selector').val()
        if(selected_program=="spl"){
            $('.notSelfPaced').hide();
        }else{
            $('.notSelfPaced').show();
        }
    }
    function installment(payment_scheme){
        c_month={{$c_month}};
        c_year={{$c_year}};
        if (payment_scheme == 'monthly'){
            if(c_month > 4){
                return 10 - (c_month-4);
            }else{
                return 10;
            }
        }        
        if (payment_scheme == 'quarterly'){
            return 3;
        }
    }
    function generateInstallment(){
        var monthlyInstallmentHtml="";
        var quaterlyInstallmentHtml="";
        //Monthly
        montlyInstCount=(installment("monthly"));
        var instAmt=(varAmt/montlyInstCount);
        var monfirstInst=(regAmt+seatAmt)+instAmt;
        monthlyInstallmentHtml+=`<p class="mb-0"><b>Installment 1:</b> ₹`+monfirstInst.toFixed(2)+`</p>`;
        for (let index = 0; index < montlyInstCount-1; index++) {
            var instCount=index+2
            monthlyInstallmentHtml+=`<p class="mb-0"><b>Installment `+instCount+`:</b> ₹`+instAmt.toFixed(2)+`</p>`;
        }
        $('#monthlyInstDiv').html(monthlyInstallmentHtml);

        quaterlyInstCount=(installment("quarterly"));
        var instAmt=(varAmt/quaterlyInstCount);
        var quafirstInst=(regAmt+seatAmt)+instAmt;
        quaterlyInstallmentHtml+=`<p class="mb-0"><b>Installment 1:</b> ₹`+quafirstInst.toFixed(2)+`</p>`;
        for (let index = 0; index < quaterlyInstCount-1; index++) {
            var instCount=index+2
            quaterlyInstallmentHtml+=`<p class="mb-0"><b>Installment `+instCount+`:</b> ₹`+instAmt.toFixed(2)+`</p>`;
        }
        $('#quaterlyInstDiv').html(quaterlyInstallmentHtml);
        payableOneTime=(regAmt+seatAmt+varAmt).toFixed(2);
        payableQuaterly=quafirstInst.toFixed(2);
        payableMonthly=monfirstInst.toFixed(2);
        $('#lbl_onetimeAmt').text("Pay ₹"+payableOneTime)
        $('#lbl_quaterlyAmt').text("Pay ₹"+payableQuaterly)
        $('#lbl_monthlyAmt').text("Pay ₹"+payableMonthly)
    }
    function paymentClick(paymentType){
        var stuName = $('#txt_stu_name').val();
        var stuMobile = $('#txt_stu_mobile').val();
        var stuEmail = $('#txt_stu_email').val();
        var stuClass = $('#class_selector').val();
        var stuProgram = $('#program_selector').val();
        var stuSubj = $('#subj_selector').val();
        var stuCourse = $('#course_selector').val();
        var stuDuration = $('#duration_selector').val();

        if(stuName && stuMobile && stuEmail && stuClass && stuProgram && stuSubj && stuCourse && stuDuration){
            $('#mdl_loader').modal('show');
            setTimeout(() => {
                    var alldata={
                    "personal_contact":stuMobile,
                    "student_name":stuName,
                    "email":stuEmail,
                    "school":"",
                    "coe":"",
                    "class":stuClass,
                    "program":stuProgram,
                    "course":stuCourse,
                    "duration":stuDuration,
                    "payment_scheme":paymentType,
                }
                var resSaveStudentDetails=saveStudentDetails(alldata);
                if(resSaveStudentDetails.response_code==200){
                    feesId=resSaveStudentDetails.response_data.fee_id;
                    prodsSent = [];
                    prodsSent.push({
                        "item_id": stuCourse,
                        "item_type": "course",
                        "item_quantity": "1",
                        "amount": "1",
                    });
                    var finalData = {
                        "userDetails": {
                            "user_name": stuName,
                            "user_email": stuEmail,
                            "user_phoneNo": stuMobile,
                            "fee_id": feesId,
                        },  
                        "prodDetails": prodsSent,
                        "device_type": 'web',
                    }
                    var resNewInitiateOrder=newInitiateOrder(finalData);
                    formHtml="";
                    if(resNewInitiateOrder.response_code==200){
                        var formdata = resNewInitiateOrder.response_data;
                        formHtml += '<form action="' + formdata.action + '" id="payment_form_submit" method="post">';
                        formHtml += '    <input type="" id="surl" name="surl" value="' + formdata.surl + '">';
                        formHtml += '    <input type="" id="furl" name="furl" value="' + formdata.furl + '">';
                        formHtml += '    <input type="" id="curl" name="curl" value="' + formdata.curl + '">';
                        formHtml += '    <input type="" id="key" name="key" value="' + formdata.key + '">';
                        formHtml += '    <input type="" id="txnid" name="txnid" value="' + formdata.txnid + '">';
                        formHtml += '    <input type="" id="amount" name="amount" value="' + formdata.amount + '">';
                        formHtml += '    <input type="" id="udf1" name="udf1" value="' + formdata.udf1 + '">';
                        formHtml += '    <input type="" id="udf2" name="udf2" value="' + formdata.udf2 + '">';
                        formHtml += '    <input type="" id="productinfo" name="productinfo" value="' + formdata.productinfo + '">';
                        formHtml += '    <input type="" id="firstname" name="firstname" value="' + formdata.firstname + '">';
                        formHtml += '    <input type="" id="email" name="email" value="' + formdata.email + '">';
                        formHtml += '    <input type="" id="phone" name="phone" value="' + formdata.phone + '">';
                        formHtml += '    <input type="" id="hash" name="hash" value="' + formdata.hash + '">';
                        formHtml += '</form>';
                        $('#payUform').html(formHtml);
                        document.getElementById("payment_form_submit").submit();
                    }else{
                        $('.error').addClass('text-danger').html(response.response_msg)
                    }
                }else{
                    toast("err", resSaveStudentDetails.response_msg)
                } 
            }, 500);
        }else{
            toast("err", "Please Fill The Mandatory Fields")
        }
        
    }
    function saveStudentDetails(alldata) {
        return $.parseJSON($.ajax({
            type: "POST",
            url: "https://crm.edudigm.com/api/save-student-online",
            data: JSON.stringify(alldata),
            dataType: "json",
            async: !1,
            headers: {
                "Content-Type": "application/json"
            }
        }).responseText)
    }
    function newInitiateOrder(alldata) {
        return $.parseJSON($.ajax({
            type: "POST",
            url: "https://crm.edudigm.com/api/new-initiate-order",
            data: JSON.stringify(alldata),
            dataType: "json",
            async: !1,
            headers: {
                "Content-Type": "application/json"
            }
        }).responseText)
    }
</script>