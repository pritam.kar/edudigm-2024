var selfPaced = [3066,3067,3068,3069];
var guided = [3062,3063,3064,3065];
function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
}
function validateMobile($mobile) {
    var mobileReg = /^\d{10}$/;
    return mobileReg.test($mobile);
}
$(document).on("input", ".email", function () {
    var value = $(this).val();
    if (value) {
        if (!validateEmail(value)) {
            $(this).css('border-color', '#ccc');
            $(this).parent().find('small').remove();
            $(this).parent().append('<small style="color: red !important;font-size: 10px;margin-bottom: 0;text-align:left;">Please Enter A Valid Email</small>');
        } else {
            $(this).css('border-color', '#008000');
            $(this).parent().find('small').remove();
        }
    } else {
        $(this).css('border-color', '#ccc');
    }
});
$(document).on("input", ".name", function () {
    var value = $(this).val();
    if (value && value.length > 2) {
        $(this).css('border-color', '#ccc');
        $(this).parent().find('small').remove();
    } else {
        $(this).css('border-color', '#ccc');
        $(this).parent().find('small').remove();
        $(this).parent().append('<small style="color: red !important;font-size: 10px;margin-bottom: 0;text-align:left;">Please Enter A Valid Name</small>');
    }
});
$(document).on("input", ".mobile", function () {
    var value = $(this).val();
    if (value != "") {
        if (!validateMobile(value)) {
            $(this).css('border-color', '#ccc');
            $(this).parent().find('small').remove();
            $(this).parent().append('<small style="color: red !important;font-size: 10px;margin-bottom: 0;text-align:left;">Please Enter A Valid Mobile</small>');
        } else {
            $(this).css('border-color', '#008000');
            $(this).parent().find('small').remove();
        }
    } else {
        $(this).css('border-color', '#ccc');
    }
});
$(document).on("blur", ".emptyValidate", function () {
    var value = "";
    value = $(this).val();
    if (value) {
        $(this).css('border-color', 'green');
        $(this).parent().find('small').remove();
    } else {
        $(this).css('border-color', '#ccc');
        $(this).parent().find('small').remove();
        $(this).parent().append('<small style="color: red !important;font-size: 10px;margin-bottom: 0;text-align:left;">Above Field Is Mandatory</small>');
    }
});
$('.emptyValidate').on('click', function () {
    var value = "";
    value = $(this).val();
    if (value) {
        $(this).css('border-color', 'green');
        $(this).parent().find('small').remove();
    } else {
        $(this).css('border-color', '#ccc');
        $(this).parent().find('small').remove();
        $(this).parent().append('<small style="color: red !important;font-size: 10px;margin-bottom: 0;text-align:left;">Above Field Is Mandatory</small>');
    }
});
function subject(shortCode){
    if(shortCode=="MS"){
        return "Mathematics, Science"
    }
    if(shortCode=="PCMB"){
        return "Physics, Chemistry, Mathematics, Biology"
    }
    if(shortCode=="PCB"){
        return "Physics, Chemistry, Biology"
    }
    if(shortCode=="PCM"){
        return "Physics, Chemistry, Mathematics"
    }
}
function getSubjectCombination(studentClass){
    if(studentClass=="III" || studentClass=="IV" || studentClass=="V"){
        return "MS"
    }
    if(studentClass=="VI" || studentClass=="VII" || studentClass=="VIII" || studentClass=="IX" || studentClass=="X"){
        return "PCMB"
    }
}
function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        if (params_arr.length) rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}
function getUrlParam(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
    return false;
}
function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}
function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}
function toast(type, toastHtml) {
    var timeInt = 1500;
    type = type.toLowerCase();
    if (type == "inf") {
        $.toast({
            heading: 'Information',
            icon: 'info',
            text: toastHtml,
            position: 'top-right',
            stack: false,
            showHideTransition: 'slide',
            hideAfter: timeInt,
            allowToastClose: true,
            stack: false
        })
    }
    if (type == "err") {
        $.toast({
            heading: 'Error',
            icon: 'error',
            text: toastHtml,
            position: 'top-right',
            stack: false,
            showHideTransition: 'slide',
            hideAfter: timeInt,
            allowToastClose: true,
            stack: false
        })
    }
    if (type == "war") {
        $.toast({
            heading: 'Warning',
            icon: 'warning',
            text: toastHtml,
            position: 'top-right',
            stack: false,
            showHideTransition: 'slide',
            hideAfter: timeInt,
            allowToastClose: true,
            stack: false
        })
    }
    if (type == "suc") {
        $.toast({
            heading: 'Success',
            icon: 'success',
            text: toastHtml,
            position: 'top-right',
            stack: false,
            showHideTransition: 'slide',
            hideAfter: timeInt,
            allowToastClose: true,
            stack: false
        })
    }
}
async function postData(url = "", data = {},methods,token) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: methods, // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, *cors, same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        dataType : 'json',
        headers: {
            "Content-Type": "application/json",
            "X-CSRF-TOKEN": token
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: "follow", // manual, *follow, error
        referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    });
    return response.json(); // parses JSON response into native JavaScript objects
}
async function postDataNoToken(url = "", data = {},methods) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: methods, // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, *cors, same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        dataType : 'json',
        headers: {
            "Content-Type": "application/json",
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: "follow", // manual, *follow, error
        referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    });
    return response.json(); // parses JSON response into native JavaScript objects
}
async function postDataNoDataToken(url = "", methods) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: methods, // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, *cors, same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        dataType : 'json',
        headers: {
            "Content-Type": "application/json",
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: "follow", // manual, *follow, error
        referrerPolicy: "no-referrer", 
    });
    return response.json(); // parses JSON response into native JavaScript objects
}