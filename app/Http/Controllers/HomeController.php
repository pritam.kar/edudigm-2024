<?php

namespace App\Http\Controllers;

use App\Models\WebContent;
use App\Models\WebOtherImage;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function view($pagename=null){
        if(isset($pagename)){
            $allData = WebContent::where([['title',$pagename]])->get();
            return view('allPages',compact('pagename','allData'));
        }else{
            return view('welcome');
        }
    }
    public function getAllmenu(){
        return WebContent::where([['is_active',1]])->pluck('title');
    }
    public function getAllImages(){
        return WebOtherImage::where([['is_active',1]])->get();
    }
}
