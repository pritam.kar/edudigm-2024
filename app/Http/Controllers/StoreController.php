<?php

namespace App\Http\Controllers;

use App\Models\ContactForm;
use App\Models\Deliverable;
use App\Models\Faq;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductDeliverable;
use App\Models\ProductFaq;
use App\Models\ProgramFaq;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class StoreController extends Controller
{
    public function productList(Request $request){
        $categoryId=$request->categoryid;
        $selectedClass = '';
        if(isset($_COOKIE['selectedClass'])) {
            $selectedClass = $_COOKIE['selectedClass'];
        }
        $products=Product::where('is_active',1)->where('is_saleable',1);
        
        if (isset($selectedClass) && $selectedClass!='') {            
            $products=$products->where("class",$selectedClass);
        }
        if (isset($request->categoryid)) {
            $products=$products->where('category_id',$categoryId);
        }
        $maxPrice= $products->max('price');
        if (isset($request->pricefrom)) {
            $products=$products->where('price','>=',$request->pricefrom);
        }
        if (isset($request->priceto)) {
            $products=$products->where('price','<=',$request->priceto);
        }
        $products=$products->get();
        
        $data=(object) [
            "pricefrom" => $request->pricefrom,
            "priceto" => $request->priceto,
            "maxPrice" => $maxPrice,
            "products" => $products
        ];
        return view('productList')->with("data",$data);
    }
    public function productDetails(Request $request){    
        $product=Product::where('is_active',1)->where('is_saleable',1)->where("id",$request->id)->get();
        $deliverablesMapId= ProductDeliverable::where('product_id',$request->id)->pluck('deliverable_id');
        $deliverables   = Deliverable::whereIn('id', $deliverablesMapId)->get();
        $faqmapid=ProductFaq::where('product_id',$request->id)->pluck('faq_id');
        $faqs   = Faq::whereIn('id', $faqmapid)->get();
        return view('productDetails',compact('product','deliverables','faqs'));
    }
    public function getAllCategories(){        
        return ProductCategory::where('is_active',1)->get();
    }
    public function cart(){
        return view('cart');
    }
    public function getAllCart(Request $request){
        $myArray = explode(',', $request->allIds);
        $product=Product::whereIn('id',$myArray)->get();
        return $product;
    }
    public function storeContactUs(Request $request){
        $contactForm = new ContactForm();
        $contactForm->person_name = $request->ContactName;
        $contactForm->person_email = $request->ContactEmail;
        $contactForm->contact_no = $request->ContactMobile;
        $contactForm->message = $request->ContactQuery;
        $contactForm->form_type = $request->ContactPageLink;
        $contactForm->save();
        return (object) [
            "response_code" => 200,
            "response_msg" => "Data Saved Successfully"
        ];
        
    }
}