<?php

namespace App\Http\Controllers;

use App\Models\CertificateGenerator;
use Illuminate\Http\Request;

class CertificateController extends Controller
{
    public function certificateGeneratorPreview($productName=null,$eventName=null){
        $CertificateGenerator = CertificateGenerator::where('product_name',$productName)->where('event_name',$eventName)->where('website','edudigm')->first();
        return view('certificate.certificate',compact('CertificateGenerator'));
    }
}
