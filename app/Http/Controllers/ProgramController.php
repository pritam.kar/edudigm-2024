<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Deliverable;
use App\Models\Faq;
use App\Models\ProductDeliverable;
use App\Models\ProgramDeliverable;
use App\Models\ProgramFaq;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    public function program($class){         
        return view('program');
    }
    public function getAllProgram($class){ 
        if(!isset($class)){
            return [
                "res_code" => 400,
                "res_message" => "Class Required"
            ];
        }
        $all_course = Course::with('course_combinations')->where([['class',$class],['id','>=', 487]])->get();
        return $all_course;
    }
    public function getCourses(Request $request){
       
        if($request->program == "gl")
            $course_mode_ids = [3062,3063,3064,3065];
        else
            $course_mode_ids = [3066,3067,3068,3069];
        $courses =
        Course::with('course_combinations')->where([['id','>=',488],['class',$request->class_name],['subjects_covered',$request->subject]])
        ->whereIn('course_mode_id',$course_mode_ids)->get();
        $all_courses = [];
        foreach($courses as $course){
            $course_combinations = [];
            foreach($course->course_combinations as $com){
                $co_ss = Course::whereIn('id',explode(',',$com->courses_attached))->get();
                $total_fees = ($co_ss[0]->reg_fee + $co_ss->sum('seat_booking_fee') +$co_ss->sum('variable_fees'));
                $course_combinations[] = (object)[
                    "id" => $com->id,
                    "duration" => $com->duration,
                    "total_fees" => $total_fees,
                    "variable_fees" => $co_ss->sum('variable_fees'),
                    "seat_booking_fee" => $co_ss->sum('seat_booking_fee'),
                    "reg_fee" => $co_ss[0]->reg_fee
                ];
            };
            $all_courses [] =(object) [
                "id" => $course->id,
                "name" => $course->name,
                "course_combinations"=> $course_combinations
            ];
        }

         return response()->json([
            'response_code' => 200,
            'response_msg' => 'get courses',
            'response_data' => $all_courses
        ]);
    }
    public function programDetails($id){     
        if(!isset($id)){
            return [
                "res_code" => 400,
                "res_message" => "Id Required"
            ];
        }
        $course_details = Course::with('course_combinations')->find($id); 
        $deliverablesMapId= ProgramDeliverable::where('course_id',$id)->pluck('deliverable_id');
        $deliverables   = Deliverable::whereIn('id', $deliverablesMapId)->get();
        $faqmapid=ProgramFaq::where('course_id',$id)->pluck('faq_id');
        $faqs   = Faq::whereIn('id', $faqmapid)->get();
        return view('programDetails',compact('course_details','deliverables','faqs'));
    }
}
