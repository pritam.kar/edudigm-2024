<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Deliverable extends Model
{
    use HasFactory;

    public function appliedOnproduct(){
        return $this->hasmany('App\Models\ProductDeliverable', 'deliverable_id');
    }
}
