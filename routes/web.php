<?php

use App\Http\Controllers\CertificateController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProgramController;
use App\Http\Controllers\StoreController;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
Route::get('/', function () {
    return view('welcome');
});
Route::get('/pages/{pageName?}', [HomeController::class, 'view'])->name('homeView');
Route::get('/get-all-menu', [HomeController::class, 'getAllmenu'])->name('getAllmenu');
Route::get('/get-all-images', [HomeController::class, 'getAllImages'])->name('getAllImages');
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/admission', function () {
    return view('admission')->with("c_month",Carbon::now()->format('m'))->with("c_year",Carbon::now()->format('Y'));
});
Route::get('/program/{class}', [ProgramController::class, 'program'])->name('program');
Route::get('/get-all-program/{class}', [ProgramController::class, 'getAllProgram'])->name('getAllProgram');
Route::post('/get-all-program-admission', [ProgramController::class, 'getCourses'])->name('getCourses');
Route::get('/program-details/{id}', [ProgramController::class, 'programDetails'])->name('program');
Route::get('/product-list/{name?}', [StoreController::class, 'productList'])->name('productList');
Route::get('/product-details/{name?}', [StoreController::class, 'productDetails'])->name('productDetails');
Route::get('/cart', [StoreController::class, 'cart'])->name('cart');
Route::get('/get-all-categories', [StoreController::class, 'getAllCategories'])->name('getAllCategories');
Route::post('/get-all-cart', [StoreController::class, 'getAllCart'])->name('getAllCart');
Route::post('/contact-form-save', [StoreController::class, 'storeContactUs'])->name('storeContactUs');
Route::any('/payment/{id?}', function () {
    return view('paymentInstallmentStructure');
});
Route::any('/invoice', function () {
    return view('newinvoice');
});
Route::any('/secret-sauce/certificate', function () {
    return view('certificate.secretSauce');
});

Route::any('certificate/{productName?}/{eventName?}', [CertificateController::class, 'certificateGeneratorPreview'])->name('certificateGeneratorPreview');